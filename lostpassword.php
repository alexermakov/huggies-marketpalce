<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>
            <div class="page__form_block__wrap">
                <div class="page__form_block page__form_password">
                    <div class="callback_title">Восстановление пароля</div>
                    <div class="lost_password_wrap">
                        <form action="">
                                <div class="field_item">
                                    <label class="field_item_label">
                                        <div class="field_item__title">
                                            Email<span class="field_required">*</span>
                                        </div>
                                        <input name="Email" placeholder="Введите email" type="text">
                                        <div class="field_item__message field_item__message--error">
                                            <div class="field_item__message__title">Это поле обязательно.</div>
                                        </div>
                                    </label>
                                </div>
                            
                                <div class="field_item field_item--submit">
                                    <button class="btn_default btn_red">Войти</button>
                                </div>
                        </form>
                    </div>   
                </div>
            </div>
            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
