<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>

            <?php include 'parts/myaccount/header.php'; ?>

            <?php include 'parts/myaccount/menu.php'; ?>

            <div class="myaccount__content">
                <div class="container">
                    <div class="myaccount__block myaccount__block__first">
                        <div class="myaccount__block__title myaccount__block__title__2">Доступные хаги</div>
                        <div class="my_hugs">
                            <div class="my_hugs__count_hugs_for_action_block">
                                <div class="my_hugs__count_hugs_for_action_block_wrap">
                                    <div class="my_hugs__count_hugs_for_action_block_title">
                                        <span class="my_hugs__top_opacity">Накоплено за действия</span> <img
                                            src="images/icons/icon_idea.svg"></div>
                                    <div class="my_hugs__count_hugs_for_action_block_val">100</div>
                                </div>
                            </div>
                            <div class="my_hugs__top">
                                <div class="my_hugs__top_col my_hugs__top_col_1">Магазин</div>
                                <div class="my_hugs__top_col my_hugs__top_col_2"><span
                                        class="my_hugs__top_opacity">Накоплено с чеков</span> <img
                                        src="images/icons/icon_idea.svg"></div>
                                <div class="my_hugs__top_col my_hugs__top_col_4"><span
                                        class="my_hugs__top_opacity">Накоплено с действий</span> <img
                                        src="images/icons/icon_idea.svg"></div>
                                <div class="my_hugs__top_col my_hugs__top_col_3">Сколько хагов и на что я могу
                                    потратить?</div>

                            </div>
                            <div class="my_hugs__body">
                                <div class="my_hugs__tr">
                                    <div class="my_hugs__tr_info">
                                        <div class="my_hugs__image">
                                            <img src="images/logo/partners/1.svg">
                                        </div>
                                        <div class="my_hugs__count_hugs">25 <span class="my_hugs__count_hugs_">хагов</span></div>
                                    </div>
                                    <div class="my_hugs__count_spend_block">
                                        <div class="my_hugs__count_spend_text">
                                            <div class="my_hugs__count_spend_count">125 хагов </div>на
                                        </div>
                                        <a href="#" class="btn_default btn_gold btn_spend_hug">подарки детского мира</a>
                                    </div>
                                </div>

                                <div class="my_hugs__tr my_hugs--deactive">
                                    <div class="my_hugs__tr_info">
                                        <div class="my_hugs__image">
                                            <div class="my_hugs--deactive__message">Чтобы активировать подарки Озон, загрузите хотя бы один чек из Озона</div>
                                            <img src="images/logo/partners/2.svg">
                                        </div>
                                        <div class="my_hugs__count_hugs">0<span class="my_hugs__count_hugs_">хагов</span></div>
                                    </div>
                                    <div class="my_hugs__count_spend_block">
                                        <div class="my_hugs__count_spend_text">
                                            <div class="my_hugs__count_spend_count">0 хагов </div>на
                                        </div>
                                        <a href="#" class="btn_default btn_gold btn_spend_hug">подарки ozon</a>
                                    </div>
                                </div>
                                <div class="my_hugs__tr my_hugs__tr_all">
                                    <div class="my_hugs__tr_info">
                                        <div class="my_hugs__image">
                                            <div class="my_hugs__image_text">Всего хагов <img
                                                    src="images/icons/icon_idea.svg"></div>
                                        </div>
                                        <div class="my_hugs__count_hugs">125 <span
                                                class="my_hugs__count_hugs_">хагов</span></div>

                                    </div>
                                    <div class="my_hugs__count_spend_block">
                                        <div class="my_hugs__count_spend_text">
                                            <div class="my_hugs__count_spend_count">125 хага </div>на
                                        </div>
                                        <a href="#" class="btn_default btn_gold btn_spend_hug">подарки HUggies</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <a href="javascript:void(0)" class="btn_show_history_hugs js_btn_show_history_hugs">История
                            накоплений и списаний</a>
                            <?php include 'parts/myaccount/history.php'; ?>

                        
                    </div>

                    <div class="myaccount__block">
                        <div class="myaccount__block__progress__title">Ваш прогресс в программе Мой Huggies:
                            <span>Активная мама Huggies</span></div>
                        <div class="myaccount__block__progress">
                            <div class="myaccount__block__progress__item active">
                                <div class="myaccount__block__progress__item_name">Мама <br>Huggies</div>
                                <img src="images/icons/star.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">1 месяц</div>
                            </div>
                            <div class="myaccount__block__progress__item active">
                                <img src="images/icons/star.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">2 месяц</div>
                            </div>
                            <div class="myaccount__block__progress__item active">
                                <div
                                    class="myaccount__block__progress__item_name myaccount__block__progress__item_name--active">
                                    Активная мама<br> Huggies </div>
                                <img src="images/icons/star_2.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">3 месяц</div>
                            </div>
                            <div class="myaccount__block__progress__item">
                                <img src="images/icons/star.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">4 месяц</div>
                            </div>
                            <div class="myaccount__block__progress__item">
                                <div class="myaccount__block__progress__item_name">Мама <br>Huggies</div>
                                <img src="images/icons/star.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">5 месяц</div>
                            </div>
                            <div class="myaccount__block__progress__item">
                                <div class="myaccount__block__progress__item_name">Супер-мама<br> Huggies</div>
                                <img src="images/icons/star.svg" class='myaccount__block__progress__item_star'>
                                <div class="myaccount__block__progress__item_period">6 месяц</div>
                            </div>
                        </div>
                        <div class="myaccount__block__progress__title_2">Загружайте чеки каждый календарный месяц,
                            станьте Супер-мамой Huggies и получите специальный приз!</div>
                    </div>

                </div>
            </div>







            <?php include 'parts/notification.php'; ?>
            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
