<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>

            <?php include 'parts/myaccount/header.php'; ?>

            <?php include 'parts/myaccount/menu.php'; ?>

            <div class="myaccount__content">
                <div class="container">
                    <div class="myaccount__block myaccount__block__first my_account__exchange">
                        <div class="myaccount__exchange__wrap">
                            <img src="images/icons/my_account/load_check.svg" class='myaccount__exchange__image'>
                            <div class="myaccount__exchange__info">
                                <div class="myaccount__exchange__title">Загружайте чеки и зарабатывайте дополнительные Хаги</div>
                                <div class="myaccount__exchange__text">
                                    <p>Получайте 1 хаг за каждые 100 рублей, потраченные на продукцию Huggies. Получайте дополнительные хаги за приглашение друзей, отзывы и заполнение анкеты</p>
                                    <a href="#" class="btn_default btn_red btn_myaccount__exchange js_btn_load_check_my_account">Загрузка чека</a>
                                    <a href="#" class="btn_default btn_myaccount__exchange btn_myaccount__exchange--2 js_btn_show_add_hugs_block">Дополнительные хаги</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php include 'parts/myaccount/load_check.php'; ?>
                    <?php include 'parts/myaccount/add_hugs.php'; ?>
                </div>
            </div>

            

            




            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
