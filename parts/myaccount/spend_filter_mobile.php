<div class="myaccount__block_spend_mobile">
    <div class="myaccount__block_spend_mobile_view">
        <div class="myaccount__spend_mobile_view_value js_btn_filter_catalog">Детский Мир<b>103 Хага</b></div>
        <div class="myaccount__spend_mobile_view_btn js_btn_filter_catalog"></div>
    </div>
    <div class="myaccount__block_spend_mobile__body js_filter__spend_mobile__body">
        <div class="spend_mobile__body_top">
            <div class="spend_mobile__body_title">
                <span>У вас доступно</span>
                <span class="idea_block">
                    <img src="images/icons/icon_idea.svg">
                    <div class="idea_block__info">
                        <b>Подсказка:</b>
                        <p>посмотреть, как копятся и на что тратятся Хаги, можно <a href="">здесь</a></p>
                    </div>
                </span>
            </div>
            <div class="btn_close_mobile_filter js_btn_close_mobile_filter"></div>
        </div>
        <div class="spend_mobile__body_shop_list_wrap">
            <div class="spend_mobile__body_shop_list_view">
                <a href="javascript:void(0)" data-index='1'>
                    <img src="images/logo/logo--huggies.png">
                    <span class="spend_mobile__count">500<span class="spend_mobile__count_text">хагов</span></span>
                </a>
                <a href="" class="btn_spend_mobile__body_shop_list_drop js_btn_spend_mobile__body_shop_list_drop"></a>
            </div>
            <div class="spend_mobile__body_shop_list js_spend_mobile__body_shop_list">
                <a href="javascript:void(0)" data-index='2'>
                    <img src="images/logo/partners/1.svg">
                    <span class="spend_mobile__count">1000<span class="spend_mobile__count_text">хагов</span></span>
                </a>
                <a href="javascript:void(0)" data-index='3'>
                    <img src="images/logo/partners/2.svg">
                    <span class="spend_mobile__count">2000<span class="spend_mobile__count_text">хагов</span></span>
                </a>
                <div class='spend_mobile__total_row'>
                    <div class="spend_mobile__total_title">Всего хагов</div>
                    <span class="spend_mobile__count">28<span class="spend_mobile__count_text">хагов</span></span>
                </div>
            </div>
        </div>
        <div class="myaccount__block_spend_filter__block">
            <div class="myaccount__block_spend_filter__title">
                <span>Стоимость подарков</span>
            </div>
            <div class="myaccount__block_spend_filter__form">
                <label class="myaccount__block_spend_filter__item">
                    <input type="radio" name="count" value="20">
                    <span class="myaccount__block_spend_filter__item_view"></span>
                    <span class="myaccount__block_spend_filter__item_text">20 хагов</span>
                </label>
                <label class="myaccount__block_spend_filter__item">
                    <input type="radio" name="count" value="50">
                    <span class="myaccount__block_spend_filter__item_view"></span>
                    <span class="myaccount__block_spend_filter__item_text">50 хагов</span>
                </label>
                <label class="myaccount__block_spend_filter__item">
                    <input type="radio" name="count" value="200">
                    <span class="myaccount__block_spend_filter__item_view"></span>
                    <span class="myaccount__block_spend_filter__item_text">200 хагов</span>
                </label>
            </div>
        </div>
    </div>
</div>