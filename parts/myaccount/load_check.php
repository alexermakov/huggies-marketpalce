<div class="myaccount__load_check myaccount__block" style='display:none'>
    <div class="myaccount__load_check__wrap">
        <div class="myaccount__load_check__form">
            <form action="/user/receipts/upload" enctype="multipart/form-data" id="upload-form" method="post">
                <label class="myaccount__load_form_label">
                    <div class='myaccount__load_form_info'>
                        <div class="myaccount__load_form_info_text">Перетащите сюда изображение чека <br> или</div>
                        <div class="btn_default btn_ghost btn_load_check_mobile">загрузите с компьютера</div>
                        <input id="receipt-file" type="file" name="File" accept="image/*">
                    </div>
                </label>
            </form>
        </div>
        <div class="myaccount_load_info">
            <div class="myaccount_load_info_title">Загрузите чек корректно</div>
            <div class="myaccount_load_info_text_wrap">
                <p>Убедитесь, что:</p>
                <ul class="myaccount_load_info_text_list">
                    <li>
                        <span class="myaccount_load_info_point">1.</span>
                        <span class="myaccount_load_info_text">QR-код хорошо виден <img
                                src="images/qr_code_load.svg"></span>
                    </li>
                    <li>
                        <span class="myaccount_load_info_point">2.</span>
                        <span class="myaccount_load_info_text">Чек содержит продукцию Huggies</span>
                    </li>
                    <li>
                        <span class="myaccount_load_info_point">3.</span>
                        <span class="myaccount_load_info_text"> Покупка совершена <em class="load_check_red_text">с 15.06.19 по 27.02.20</em></span>
                    </li>
                    <li>
                        <span class="myaccount_load_info_point">4.</span>
                        <span class="myaccount_load_info_text"> Покупка на сайтах detmir.ru, ozon.ru, или в магазинах сети "Детский мир"<img
                                src="images/icons/icon_idea.svg" class='myaccount_load__icon_idea'></span>
                    </li>
                    <li>
                        <span class="myaccount_load_info_point">5.</span>
                        <span class="myaccount_load_info_text">Ранее чек не использовался</span>
                    </li>
                </ul>
            </div>
            <div class="btn_default btn_red btn_load_check_send">Отправить чек</div>
        </div>
    </div>
</div>
