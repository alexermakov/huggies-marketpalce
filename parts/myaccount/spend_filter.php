<div class="myaccount__block_spend_filter">
    <div class="myaccount__block_spend_filter__block">
        <div class="myaccount__block_spend_filter__title">
            <span>У вас доступно</span>
            <span class="idea_block">
                <img src="images/icons/icon_idea.svg">
                <div class="idea_block__info">
                    <b>Подсказка:</b>
                    <p>посмотреть, как копятся и на что тратятся Хаги, можно <a href="">здесь</a></p>
                </div>
            </span>
        </div>
        <div class="myaccount__block_spend_filter__shop">
            <img src="images/logo/partners/1.svg">
            <div class="myaccount__block_spend_filter__shop__hugs">1 000 хагов</div>
        </div>
    </div>
    <div class="myaccount__block_spend_filter__block">
        <div class="myaccount__block_spend_filter__title">
            <span>Стоимость подарков</span>
        </div>
        <div class="myaccount__block_spend_filter__form">
            <label class="myaccount__block_spend_filter__item">
                <input type="radio" name="count" value="20">
                <span class="myaccount__block_spend_filter__item_view"></span>
                <span class="myaccount__block_spend_filter__item_text">20 хагов</span>
            </label>
            <label class="myaccount__block_spend_filter__item">
                <input type="radio" name="count" value="50">
                <span class="myaccount__block_spend_filter__item_view"></span>
                <span class="myaccount__block_spend_filter__item_text">50 хагов</span>
            </label>
            <label class="myaccount__block_spend_filter__item">
                <input type="radio" name="count" value="200">
                <span class="myaccount__block_spend_filter__item_view"></span>
                <span class="myaccount__block_spend_filter__item_text">200 хагов</span>
            </label>
        </div>
    </div>
</div>