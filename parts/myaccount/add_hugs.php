<div class="myaccount__block__additional_hugs" style='display:none'>
    <div class="myaccount__block_row myaccount__block_row--2">
        <div class="myaccount__block">
            <div class="myaccount__block__title">Отправить отзыв</div>
            <div class="myaccount__block__add_review_top">
                <img src="images/icons/heart.svg" class='myaccount__block__add_review_top__image'>
                <div class="myaccount__block__add_review_text">Пришлите фото и ссылку на отзыв и получите +2 хага за отзыв на каждом из сайтов</div>
            </div>

            <div class="myaccount__block__add_review_list">
                <div class="myaccount__block__add_review">
                    <img src="images/icons/add_review/1.svg">
                    <div class="myaccount__block__add_review_info">
                        <div class="myaccount__block__add_review_name">Отзовик</div>
                        <div class="add_review_item_hug">+2 хага</div>
                    </div>

                    <div class="myaccount__block__add_review__status">
                        <a href="#" class="add_review__status--def add_review__status--wait">Ожидает проверки</a>
                    </div>
                </div>

                <div class="myaccount__block__add_review">
                    <img src="images/icons/add_review/2.svg">
                    <div class="myaccount__block__add_review_info">
                        <div class="myaccount__block__add_review_name">IRecommend</div>
                        <div class="add_review_item_hug">+2 хага</div>
                    </div>

                    <div class="myaccount__block__add_review__status">
                        <a href="#" class="add_review__status--def add_review__status--added">Хаги зачислены</a>
                    </div>
                </div>
                <div class="myaccount__block__add_review">
                    <img src="images/icons/add_review/3.svg">
                    <div class="myaccount__block__add_review_info">
                        <div class="myaccount__block__add_review_name">Детский Мир</div>
                        <div class="add_review_item_hug">+2 хага</div>
                    </div>

                    <div class="myaccount__block__add_review__status">
                        <a href="#" class="add_review__status--def add_review__status--btn">Прикрепить отзыв</a>
                    </div>
                </div>
                <div class="myaccount__block__add_review">
                    <img src="images/icons/add_review/4.svg">
                    <div class="myaccount__block__add_review_info">
                        <div class="myaccount__block__add_review_name">Яндекс.Маркет</div>
                        <div class="add_review_item_hug">+2 хага</div>
                    </div>

                    <div class="myaccount__block__add_review__status">
                        <a href="#" class="add_review__status--def add_review__status--btn">Прикрепить отзыв</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="myaccount__block">
            <div class="myaccount__block__title">Вовлечь друга</div>
            <div class="myaccount__block__invite">
                <div class="myaccount__block__invite_col">
                    <img src="images/icons/invite_1.svg" class='myaccount__block__invite_img'>
                    <div class="invite_friend_text">Поделитесь своим уникальным кодом и получите +2 хага за каждую соц. сеть
                    </div>
                    <div class="invite_friend_social">
                        <a href='#'>
                            <img src="images/icons/social/share/vk.svg">
                        </a>
                        <a href='#'>
                            <img src="images/icons/social/share/fb.svg">
                        </a>
                        <a href='#'>
                            <img src="images/icons/social/share/ok.svg">
                        </a>
                    </div>
                </div>
                <div class="myaccount__block__invite_col">
                    <img src="images/icons/invite_2.svg" class='myaccount__block__invite_img'>
                    <div class="invite_friend_text">
                        Отправьте код другу и получите +5 хагов, когда он заработает 5 хагов за загрузку чеков
                    </div>
                    <a class="btn_default btn_red btn_send_code">Отправить</a>
                </div>
                <div class="myaccount__block__invite__full">
                    <div class="invite_friend_promocode_text">Ваш уникальный код</div>
                    <div class="invite_friend_promocode_value">94I8W9I</div>
                </div>
            </div>

        </div>
    </div>
    
    <div class="myaccount__block_row myaccount__block_row--2">

        <div class="myaccount__block">
            <div class="myaccount__block__title">Поделиться статусом
            </div>

            <div class="myaccount_share_status_wrap">
                <img src="images/icons/i_share.svg">
                <div class="myaccount_share_status_text">Поделитесь своим статусом в социальных сетях и заработайте +2 хага
                </div>
            </div>

            <div class="myaccount_share_list">
                <a href='#'>
                    <img src="images/icons/social/share/vk.svg">
                </a>
                <a href='#'>
                    <img src="images/icons/social/share/fb.svg">
                </a>
                <a href='#'>
                    <img src="images/icons/social/share/ok.svg">
                </a>
            </div>
        </div>


        <div class="myaccount__block myaccount__block_fill_profile">
            <div class="myaccount__block__title block_myaccount_body_middle_title--left">Заполнить анкету</div>
            <div class="myaccount_exchange_fill_profile_wrap">
                <div class="myaccount_exchange_fill_profile_text">Заполните анкету <br>и получите +5 хагов</div>
                <a href="/user/poll" class="btn_default btn_red btn_fill_profile">Заполнить</a>
            </div>
        </div>
    </div>
</div>
