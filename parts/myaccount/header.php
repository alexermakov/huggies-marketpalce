

<div class="myaccount__header">
    <div class="container">
        <div class="account__header_user">
            <div class="myaccount__header__user__avatar">
                <a href="/user">
                    <img src="images/icons/avatar/woomen.svg">
                </a>
            </div>

            <div class="myaccount__header__user__info">
                <div class="myaccount__header__user__name">
                    <a href="/user">Алексей Ермаков</a>
                </div>
                <div class="myaccount__header__user__hug_count">5 хагов</div>
            </div>
        </div>
        <div class="myaccount__header__badges">
            <div class="myaccount__header__badges__item">
                <img src="images/bandge/super_mother.svg">
            </div>
            <div class="myaccount__header__badges__item">
                <a href="#">
                    <img src="images/bandge/learn_how.svg">
                </a>
            </div>
        </div>
    </div>
</div>