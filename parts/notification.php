
<div class="notifications_block hide">
    <div class="notifications_block_wrap">
        <div class="notifications_block_top">
            <div class="notifications_block_title">
                Уведомления
                <span class="notifications_block_count">2</span>
            </div>
            <div class="notifications_block_close js_notifications_block_close">
                <span></span>
            </div>
        </div>
        <div class="notifications_block_list">
            <div class="notifications_item">
                <div class="notifications_item_date">Сегодня</div>
                <div class="notifications_item_list">
                    
                    <div class="notifications_el notification--not_read">
                        <div class="notifications_el_info notifications_minus_hugs">
                            <div class="notifications_el_top">- 20 хагов</div>
                            <div class="notifications_el_text"><a href="">Обмен на скидку 5%</a></div>
                        </div>
                        <div class="notifications_el_time">16:06</div>
                    </div>
                    <div class="notifications_el notification--not_read">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 2 хага</div>
                            <div class="notifications_el_text">Перевод от пользователя Анфиса</div>
                        </div>
                        <div class="notifications_el_time">15:46</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 9 хагов</div>
                            <div class="notifications_el_text">Успешная загрузка чека</div>
                        </div>
                        <div class="notifications_el_time">15:10</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info">
                            <div class="notifications_el_text">
                                Пора пополнить запасы Huggies! <br>
                                Купить на сайте <a href="" target='blank'>detmir.ru</a></div>
                        </div>
                        <div class="notifications_el_time">14:12</div>
                    </div>
                </div>
            </div>
            <!-- --- -->
            <div class="notifications_item">
                <div class="notifications_item_date">Вчера</div>
                <div class="notifications_item_list">
                    
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_minus_hugs">
                            <div class="notifications_el_top">- 20 хагов</div>
                            <div class="notifications_el_text">Обмен на подарок: Призы 1-го уровня</div>
                        </div>
                        <div class="notifications_el_time">16:06</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 2 хага</div>
                            <div class="notifications_el_text">Отзыв добавлен</div>
                        </div>
                        <div class="notifications_el_time">15:46</div>
                    </div>
                    <a href='#' class="notifications_el">
                        <div class="notifications_el_info notifications_with_image">
                            <img src="images/icons/star.svg" class="notifications_image">
                            <div class="notifications_info_image">
                                Поздравляем! <br>у вас новый статус! <br><span class="extend_text">Супер-мама</span>
                            </div>
                        </div>
                        <div class="notifications_el_time">15:10</div>
                    </a>
                    <div class="notifications_el">
                        <div class="notifications_el_info">
                            <div class="notifications_el_text">
                                Подарок! <br>
                                Мой Huggies дарит Вам <a href="" target='_blank'>промокод<br>на скидку 5%</a> в онлайн магазине <br><a href="" target='_blank'>detmir.ru</a>
                            </div>
                            <a href="" class="btn_notification">Получить промокод</a>
                        </div>
                        <div class="notifications_el_time">14:12</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 2 хага</div>
                            <div class="notifications_el_text">За заполнение анкеты на сайте</div>
                        </div>
                        <div class="notifications_el_time">11:09</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 2 хага</div>
                            <div class="notifications_el_text">Ваш друг Иван Иванов зарегистрировался!</div>
                        </div>
                        <div class="notifications_el_time">11:01</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_plus_hugs">
                            <div class="notifications_el_top">+ 5 хагов</div>
                            <div class="notifications_el_text">Ваш друг Иван Иванов зарегистрировался и загрузил чек!</div>
                        </div>
                        <div class="notifications_el_time">10:35</div>
                    </div>
                    <div class="notifications_el notifications_el--extend">
                        <div class="notifications_el_info notifications_with_image">
                            <img src="images/icons/notificaton_2.svg" class="notifications_image">
                            <div class="notifications_info_image">
                                Для вас —  <span class="extend_text">персональная скидка 50% на Huggies для новорожденных</span> — <a href="">es.huggies.ru</a>
                            </div>
                        </div>
                        <div class="notifications_el_time">15:10</div>
                    </div>
                    
                    <div class="notifications_el">
                        <div class="notifications_el_info">
                            <div class="notifications_el_text">
                                Вы получили ответ на ваше обращение.  <a href="" target='blank'></a>Прочитать ответ</a></div>
                        </div>
                        <div class="notifications_el_time">14:12</div>
                    </div>
                    
                    <div class="notifications_el">
                        <div class="notifications_el_info">
                            <div class="notifications_el_text">
                                Внимание! <br>
                                Правила акции изменились<div class="br"></div>
                                <a href="" target='blank'></a>Перейти к правилам.</a></div>
                        </div>
                        <div class="notifications_el_time">14:12</div>
                    </div>
                    <div class="notifications_el notifications_el--extend">
                        <div class="notifications_el_info notifications_with_image">
                            <img src="images/icons/notificaton_1.svg" class="notifications_image">
                            <div class="notifications_info_image">
                                Ура! <br>
                                Мы поздравляем вас с днем рождения!
                            </div>
                        </div>
                        <div class="notifications_el_time">15:10</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_minus_hugs">
                            <div class="notifications_el_top">Отзыв не принят</div>
                            <div class="notifications_el_text">Перейти к отзывам</div>
                        </div>
                        <div class="notifications_el_time">10:06</div>
                    </div>
                    <div class="notifications_el">
                        <div class="notifications_el_info notifications_minus_hugs">
                            <div class="notifications_el_top">Ошибка загрузки чека</div>
                            <div class="notifications_el_text">Перейти к загрузке</div>
                        </div>
                        <div class="notifications_el_time">10:06</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="js_notification_bg notification_bg"></div>