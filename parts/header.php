<header class="header_main js_header">
    <div class="container">
        <a href="/" class="logo_header">
            <img src="images/logo/logo--my_huggies.png">
        </a>

        <nav class="header__menu">
            <ul>
                <li><a href="index.php">Главная</a></li>
                <li><a class="js_modal_catalog" data-options="{'touch' : false}" data-fancybox data-src="#js_all_catalog_prize" href="javascript:void(0)">Каталог призов</a></li>
                <li><a href="faq.php">FAQ</a></li>
                <li><a href="rules.php">Правила</a></li>
            </ul>
        </nav>

        <div class="header__user_info">
            <div class="header__user_nologin">
                <a href="/Account/Login" class="btn_login_top">Войти в личный кабинет</a>
                <a href="/Account/Register" class="btn_default btn_gold btn_top_registration">Регистрация</a>
            </div>
            
            <div class="header__user_login" style="display:none">
                <a href="#" class="header_user__label header_user__label--notification js_open_notification">
                    <img src="images/icons/bell.svg" class='header_user__label--notification_icon'>
                    <span class="header_user__label--notification_count">2</span>
                </a>
                <a href="#" class="header_user__block">
                    <div class="header_user__label header_user__label--user">
                        <img src="images/icons/avatar/woomen.svg">
                    </div>
                    <span class="header_user__block__name">
                        Мария <br>
                        Комаровская
                    </span>
                </a>
                <a href="" class="header__user_login__exit">Выход</a>
            </div>
        </div>



        <div class="btn_mobile_menu js_btn_mobile_menu">
            <span></span>
            <span></span>
        </div>

        <div class="header_user_info__mobile">
            <a href="#" class="header_user__label header_user__label--notification js_open_notification">
                <img src="images/icons/bell.svg" class='header_user__label--notification_icon'>
                <span class="header_user__label--notification_count">2</span>
            </a>


            <a href="/Account/Login" class="mobile_login_avatar">
                <img src="images/icons/avatar/top.svg">
            </a>
        </div>


    </div>




    
</header>

<div class="mobile_menu js_mobile_menu">
        <div class="container">
            <div class="mobile_menu_wrap">
                <nav class="mobile_menu_list">
                    <ul>
                        <li><a href="/">Главная</a></li>
                        <li><a class="js_modal_catalog" data-options="{'touch' : false}" data-fancybox data-src="#js_all_catalog_prize" href="javascript:void(0)">Каталог призов</a></li>
                        <li><a href="faq.php">FAQ</a></li>
                        <li><a href="rules.php">Правила</a></li>
                        <li><a href="feedback.php">Обратная связь</a></li>
                    </ul>
                </nav>
                <div class="mobile_user_info">
                    <div class="mobile_user_info_top_no_login">
                        <a href="/Account/Login" class="mobile_user_info_top_no_login_link">Войти в личный кабинет</a>
                        <a href="/Account/Register" class="btn_default btn_gold btn_registration_mobile">Регистрация</a>
                    </div>
                    <div class="mobile__header__user_login" style="display:none">
                        <a href="#" class="header_user__block">
                            <div class="header_user__label header_user__label--user">
                                <img src="images/icons/avatar/woomen.svg">
                            </div>
                            <div class="header_user__block_wrap">
                                <span class="header_user__block__name">
                                    Мария Комаровская
                                </span>
                                <div class="header_user__block_hugs">20 хагов</div>
                            </div>
                        </a>
                        <a href="" class="mobile__header__to_buy">За покупками</a>
                        <a href="" class="mobile_header__user_login__exit">
                            <span class="mobile_header__user_exit__icon"></span>
                            <span class="mobile_header__user_exit__text">Выход</span>
                        </a>
                    </div>
                </div>
                <div class="mobile_menu_social">
                    <a href="https://vk.com/huggiesrussia" target="_blank">
                        <img src="images/icons/social/footer/vk.svg">
                    </a>
                    <a href="https://www.instagram.com/huggies_russia/" target="_blank">
                        <img src="images/icons/social/footer/you.svg">
                    </a>
                    <a href="https://www.youtube.com/channel/UCk0st750W_v-MWs7yDpSO9A" target="_blank">
                        <img src="images/icons/social/footer/inst.svg">
                    </a>    
                </div>
            </div>
        </div>
    </div>