<div class="footer_decore"></div>
<footer class="main__footer">
    <div class="container">
        <div class="footer__top">
            <a href="https://huggies.ru/" class="footer__logo">
                <img src="images/logo/logo--huggies.png">
            </a>
            <a href="/feedback" class="footer__mail">
                <span>Обратная связь</span>
                <img src="images/icons/mail.svg">
            </a>
        </div>

        <div class="footer__info">
            <ul class="footer__menu">
                <li><a href="https://www.huggies.ru/products/">Продукция Huggies</a></li>
                <li><a href="https://www.huggies.ru/actions/">Акции и скидки</a></li>
                <li><a href="https://www.huggies.ru/pochemy-huggies/">Почему Huggies?</a></li>
                <li><a href="https://www.huggies.ru/library/">Библиотека</a></li>
            </ul>
            <div class="footer__info_description">
                <p>Все наименования, логотипы и торговые марки являются собственностью Kimberly-Clark Worldwide.Inc. Copyright 2020. Все права защищены.</p>
                <p>Посещение нашего сайта и использование представленной на нем информации регулируются <a href="https://www.kimberly-clark.com/ru/terms-of-use" target="_blank" rel="noopener noreferrer">Правовыми положениями.</a> Пожалуйста, ознакомьтесь с нашей <a href="https://www.kimberly-clark.com/ru/privacy" target="_blank" rel="noopener noreferrer">Политикой конфиденциальности.</a></p>
                <p>© Disney. По мотивам произведений и работ А. А. Милна и Э. Х. Шепарда.</p>
            </div>
            <div class="footer__social">
                <div class="footer__social__title">Мы в социальных сетях</div>
                <div class="footer__social__list">
                    <a href="https://vk.com/huggiesrussia" target="_blank">
                        <img src="images/icons/social/footer/vk.svg">
                    </a>
                    <a href="https://www.instagram.com/huggies_russia/" target="_blank">
                        <img src="images/icons/social/footer/you.svg">
                    </a>
                    <a href="https://www.youtube.com/channel/UCk0st750W_v-MWs7yDpSO9A" target="_blank">
                        <img src="images/icons/social/footer/inst.svg">
                    </a>    
                </div>
            </div>  
        </div>
    </div>
</footer>

<?php include 'parts/modals.php'; ?>



<script src="js/jquery.min.js"></script>
<script src="js/jquery.selectric.min.js"></script>
<script src="js/datepicker.min.js"></script>
<script src="js/jquery.fancybox.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/main.js?3"></script>