<div class="modal__default" style="display:none" id="js_modal_1">
    <div class="modal__default__wrap">
        <div class="modal__title">Чек принят</div>
        <div class="modal__text">Спасибо, ваш чек принят и вам будет начислено </div>
        <a href="javascript:void(0)" class="btn_default btn_ghost js_btn_modal_close">закрыть</a>
    </div>
</div>

<div class="modal__default modal__big" style="display:none" id="js_modal_2">
    <div class="modal__default__wrap modal_align--left">
        <div class="modal__title modal__text--left">Чек принят</div>
        <div class="modal__text modal__text--left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Cumque beatae recusandae, minima qui perferendis eos sed totam harum iusto eaque, asperiores tempora cum error eum id non saepe consequatur! Libero.</div>
        <a href="javascript:void(0)" class="btn_default btn_ghost js_btn_modal_close">закрыть</a>
    </div>
</div>


<div class="modal__default modal_accepted_check" style="display:none" id="js_modal_accepted_check--1">
    <div class="modal__default__wrap">
        <img src="images/icons/checked.svg" class='modal_accepted_check__image'>
        <div class="modal__title">Чек принят</div>
        <div class="modal__text">Спасибо, ваш чек принят и вам будет начислено <span class="modal_decore__extend">20
                хагов</span> на счет Озона</div>
        <div class="modal_accepted_check__logo">
            <img src="images/logo/partners/1.svg">
        </div>
        <a href="javascript:void(0)"
            class="btn_default btn_ghost btn_modal_accepted_check js_btn_modal_close">закрыть</a>
    </div>
</div>

<div class="modal__default modal_accepted_check" style="display:none" id="js_modal_accepted_check--2">
    <div class="modal__default__wrap">
        <img src="images/icons/checked.svg" class='modal_accepted_check__image'>
        <div class="modal__title">Чек принят</div>
        <div class="modal__text">Спасибо, ваш чек принят и вам будет начислено <span class="modal_decore__extend">20
                хагов</span> на счет Озона</div>
        <div class="modal_accepted_check__logo">
            <img src="images/logo/partners/2.svg">
        </div>
        <a href="javascript:void(0)"
            class="btn_default btn_ghost btn_modal_accepted_check js_btn_modal_close">закрыть</a>
    </div>
</div>


<div class="modal__default modal_action_extended" style="display:none" id="js_modal_action_extended">
    <div class="modal__default__wrap__0">
        <img src="images/data/bg_modal_1.png" class='modal__title_action_extended__image'>
        <div class="modal__title_action_extended__info">
            <div class="modal__title_action_extended">Ура! <br>Акция продлеватся!</div>
            <div class="modal__text_action_extended">С 15 февраля по 15 августа участвуйте в программе лояльности,
                зарабатывайте больше хагов и обменивайте на новые подарки в каталоге</div>
            <a href="#" class="btn_default btn_gold btn_modal_action_extended">Посмотреть каталог</a>
        </div>
    </div>
</div>


<div class="modal__default modal_exchange_retail" style="display:none" id="js_modal_exchange_retail">
    <div class="modal__default__wrap">
        <img src="images/data/book.png" class='modal_exchange_retail__image'>
        <div class="modal__text">Вы хотите обменять <span class="modal_decore__extend_2">50 хагов</span> на Kreiss 3
            детская игрушка в магазине Ozon?</div>
        <div class="modal_accepted_check__logo">
            <img src="images/logo/partners/2.svg">
        </div>
        <div class="modal__btn_wrap">
            <a href="#" class="btn_default btn_ghost btn_modal_exchange_retail_1">закрыть</a>
            <a href="#" class="btn_default btn_gold btn_modal_exchange_retail_2">обменять</a>

        </div>

        <div class="modal_exchange_retail__add_text">Перед обменом, пожалуйста, убедитесь, что Ozon осуществляет
            доставку в вашем городе</div>

    </div>
</div>

<div class="modal__default modal_exchange_universal_gift" style="display:none" id="js_modal_exchange_universal_gift">
    <div class="modal__default__wrap">
        <img src="images/data/book.png" class='modal_exchange_retail__image'>

        <div class="modal__text">Вы хотите обменять <span class="modal_decore__extend_2">50 хагов</span> на
            универсальный подарок от Huggies “Name”</div>



        <div class="modal__exchange_hugs__block">
            <div class="modal__exchange_hugs__block__top">
                <div class="modal__exchange_hugs__block__th">Хаги</div>
                <div class="modal__exchange_hugs__block__th">Доступно</div>
                <div class="modal__exchange_hugs__block__th">Обменять</div>
            </div>
            <div class="modal__exchange_hugs__block__body">
                <div class="modal__exchange_hugs__block__tr">
                    <div class="modal__exchange_hugs__block__title">Huggies</div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field' disabled value="45">
                    </div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field'>
                    </div>
                </div>
                <div class="modal__exchange_hugs__block__tr">
                    <div class="modal__exchange_hugs__block__title">Детский мир</div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field' disabled value="25">
                    </div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field'>
                    </div>
                </div>
                <div class="modal__exchange_hugs__block__tr">
                    <div class="modal__exchange_hugs__block__title">Ozon</div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field' disabled value="5">
                    </div>
                    <div class="modal__exchange_hugs__block__td">
                        <input type="text" class='modal__exchange_hugs__block__field'>
                    </div>
                </div>
            </div>
            <div class="modal__exchange_hugs__block__bottom">
                <div class="modal__exchange_hugs__block__bottom_text">Выбрано Хагов</div>
                <div class="modal__exchange_hugs__block__bottom_count">50</div>
            </div>
        </div>

        <div class="modal__btn_wrap">
            <a href="#" class="btn_default btn_ghost btn_modal_exchange_retail_1">закрыть</a>
            <a href="#" class="btn_default btn_gold btn_modal_exchange_retail_2">обменять</a>
        </div>

        <div class="modal_exchange_retail__add_text">Перед обменом, пожалуйста, убедитесь, что Ozon осуществляет
            доставку в вашем городе</div>
    </div>
</div>


<div class="modal__default modal__catalog" style="display:none" id="js_all_catalog_prize">
    <div class="modal__catalog_top">
        <div class="modal__catalog_title">Каталог призов</div>
        <div class="modal__catalog_categories js_modal__catalog_categories">
            <a href="javascript:void(0)">
                <img src="images/logo/logo--huggies.png">
            </a>
            <a href="javascript:void(0)" class='active'>
                <img src="images/logo/partners/1.svg">
            </a>
            <a href="javascript:void(0)">
                <img src="images/logo/partners/2.svg">
            </a>
        </div>
    </div>
    <div class="modal__catalog_body">
        <div class="modal__catalog_item js_modal__catalog--1" style='display:none'>
            <div class="modal__catalog_item_level modal__catalog_item_level_0">
                <div class="modal__catalog_item_level--0">
                    <img src="images/data/prize/0.png">
                    <div class="modal__catalog_item_level--0_info">
                        <div class="modal__catalog_item_level--0__text">Дополнительная скидка 5% на Huggies в <span class="level_extend_text">Detmir.ru</span></div>
                        <div class="modal__catalog_item_level--0__hugs">20 хагов</div>
                        <a href="#" class="btn_default btn_exchange">Обменять</a>
                    </div>
                </div>
            </div>

            <div class="modal__catalog_item_level">
                <div class="modal__catalog_item_level__title">1 УРОВЕНЬ</div>
                <div class="js_catalog catalog_list_modal">
                    <?php for ($i=1; $i < 10; $i++) :?>
                        <div class="product_item_modal ">
                            <a href="#" class='product_image_wrap_modal'>
                                <img src="images/prize/<?= $i;?>.jpg">
                            </a>
                            <div class="product_title_modal">Игрушка Baby Go Лев</div>
                            <div class="product_title_count">50 хагов</div>
                            <a href="#" class="btn_default btn_exchange">Обменять</a>
                        </div>
                    <?php endfor;?>
                </div>
            </div>

            <div class="modal__catalog_item_level">
                <div class="modal__catalog_item_level__title">2 УРОВЕНЬ</div>
                <div class="js_catalog catalog_list_modal">
                    <?php for ($i=1; $i < 10; $i++) :?>
                        <div class="product_item_modal ">
                            <a href="#" class='product_image_wrap_modal'>
                                <img src="images/prize/<?= $i;?>.jpg">
                            </a>
                            <div class="product_title_modal">Книга Clever Первые слова Животные</div>
                            <div class="product_title_count">100 хагов</div>
                            <a href="#" class="btn_default btn_exchange">Обменять</a>
                        </div>
                    <?php endfor;?>
                </div>
            </div>



            

        </div>

        <div class="modal__catalog_item js_modal__catalog--2">
            <div class="modal__catalog_item_level modal__catalog_item_level_0">
                <div class="modal__catalog_item_level--0">
                    <img src="images/data/prize/0.png">
                    <div class="modal__catalog_item_level--0_info">
                        <div class="modal__catalog_item_level--0__text">2 Дополнительная скидка 5% на Huggies в <span class="level_extend_text">Detmir.ru</span></div>
                        <div class="modal__catalog_item_level--0__hugs">20 хагов</div>
                        <a href="#" class="btn_default btn_exchange">Обменять</a>
                    </div>
                </div>
            </div>

            <div class="modal__catalog_item_level">
                <div class="modal__catalog_item_level__title">1 УРОВЕНЬ</div>
                <div class="js_catalog catalog_list_modal">
                    <?php for ($i=1; $i < 10; $i++) :?>
                        <div class="product_item_modal ">
                            <a href="#" class='product_image_wrap_modal'>
                                <img src="images/prize/<?= $i;?>.jpg">
                            </a>
                            <div class="product_title_modal">Игрушка Baby Go Лев 2</div>
                            <div class="product_title_count">50 хагов</div>
                            <a href="#" class="btn_default btn_exchange">Обменять</a>
                        </div>
                    <?php endfor;?>
                </div>
            </div>

            <div class="modal__catalog_item_level">
                <div class="modal__catalog_item_level__title">2 УРОВЕНЬ</div>
                <div class="js_catalog catalog_list_modal">
                    <?php for ($i=1; $i < 10; $i++) :?>
                        <div class="product_item_modal ">
                            <a href="#" class='product_image_wrap_modal'>
                                <img src="images/prize/<?= $i;?>.jpg">
                            </a>
                            <div class="product_title_modal">Книга Clever Первые слова Животные</div>
                            <div class="product_title_count">100 хагов</div>
                            <a href="#" class="btn_default btn_exchange">Обменять</a>
                        </div>
                    <?php endfor;?>
                </div>
            </div>



            

        </div>

    </div>

</div>
