<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>
            <div class="faq_page">
                <div class="faq_top">
                    <div class="container">
                        <div class="faq_title">
                            <span>F.A.Q.</span><br>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="faq_page__text_wrap">
                        <div class="faq_wrap_title">Ответы на часто задаваемые вопросы</div>
                        <div class="faq_list">
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Что такое Мой Huggies?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Мой Huggies – это программа лояльности Huggies и сети магазинов «Детский Мир». Каждая ваша покупка продукции Huggies в «Детском мире» позволяет вам накапливать баллы («хаги»), чтобы получать дополнительную скидку на следующую покупку Huggies или подарки от Huggies. Для этого необходимо зарегистрироваться в программе и загружать чеки с Huggies в Личном кабинете.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Чеки принимаются только из «Детского мира»?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Да, вы можете загружать чеки из розничных магазинов «Десткий мир» и интернет-магазина <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a>.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Какой ассортимент участвует в акции?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Весь ассортимент Huggies, представленный в сети магазинов «Детский мир» и <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a></p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Что такое приветственная скидка 5% и как ее использовать?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>После завершения процедуры регистрации в программу вам на почту придет письмо с приветственной скидкой 5% на покупку Huggies в «Детском мире». Это дополнительная скидка, которая суммируется с текущими регулярными акциями на Huggies в «Детском мире». Например, вы получили промокод на скидку 5% за регистрацию в Мой Huggies, далее покупаете подгузники и/или трусики Huggies Elite Soft в «Детском мире» по акции со скидкой 35%, применяете в корзине свой промокод из Мой Huggies и стоимость вашего заказа уменьшается еще на 5%. Промокод действителен только для заказов в интернет-магазине <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a>.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Что такое Хаг?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>«Хаг» (от англ. «to hug» — обнимать) – внутренняя валюта программы лояльности «Мой Huggies». 1 хаг равен 100 рублей, потраченным на Huggies в «Детском мире». Например, вы грузите чек из Детского мира, в котором есть трусики Huggies для мальчиков 4 размер (126 шт.) стоимостью 2&nbsp;579 рублей. После проверки валидности чека, вам на счет будет начислено 25 хагов. Хаги начисляются только за каждые ровные 100 рублей. В нашем примере 25 хагов начислено за 2500 рублей, остаток 79 рублей в зачет не идет. </p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Как копить хаги?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>
                                        У вас есть много вариантов как легко накопить хаги:<br>
                                        • Загружайте чеки с Huggies из «Детского мира» и получайте 1 хаг за каждые 100 рублей, потраченные на Huggies в «Детском мире».<br>
                                        • Заполните Анкету в Личном кабинете и получите 5 хагов.<br>
                                        • Поделитесь в социальной сети и получите по 2 хага за каждую сеть ВКонтакте, Одноклассники, Facebook.<br>
                                        • Оставьте отзыв на irecommend.ru, otzovik.ru, detmir.ru и market.yandex.ru и получите по 2 хага за каждую площадку.<br>
                                        • Пригласите друга, отправив ему уникальный код любым удобным вам способом, и получите по 5 хагов за каждого друга, зарегистрировавшегося по вашему уникальному коду и загрузившего чеков в сумме на 5 хагов.<br>
                                    </p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Сколько хагов мне нужно накопить для того, чтобы получить промокод на дополнительную скидку на Huggies на следующую покупку в «Детском мире»? </div>
                                </div>
                                <div class="faq_item_info">
                                    <p>
                                        Вам нужно накопить 20 хагов. Для этого вам нужно загрузить чеков c Huggies на сумму не менее 2000 рублей. Это могут быть несколько загруженных чеков. Суммы из чеков, потраченные на Huggies, плюсуются и накапливаются. А также вы можете заработать хаги другими способами в разделе Дополнительные Хаги.
                                        <span style="text-decoration: underline">Пример как заработать 20 хагов</span>: загрузить чек с Huggies на 1100 рублей, получить за него 11 хагов;  заполнить в Личном кабинете Анкету и получить за нее еще 5 хагов; поделиться постом о программе Вконтакте и пригласить друга в программу через Whatsapp, получив по 2 хага за каждое действие. Таким образом, вы накопили: 11 + 5 + 2 + 2 = 20 хагов. Ура, теперь вы можете обменять их на скидку в разделе Обменять Хаги или продолжить зарабатывать хаги дальше, чтобы накопить 50 хагов и обменять их на классный подарок из Каталога призов.
                                    </p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Какие призы меня ждут?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Мы подготовили для вас 3 уровня волшебных призов: самые любимые всеми мамами и необходимые для познания мира и раннего развития каждому малышу книги издательства Clever, классные развивающие игрушки и каталки Baby Go, самокаты Kreiss и палатки для игры дома и на даче. Покупайте Huggies и грузите ваши чеки в Личном кабинете, копите хаги и выбирайте в Каталоге призов подарок для вашего малыша! Это очень просто!</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Сколько будет действовать акция?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Срок проведения акции продлён до 1-го ноября 2020 года.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">В каких городах действует акция?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Акция действует на всей территории Российской Федерации, в сети магазинов «Детский Мир» и на сайте <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a>.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Как мне получить мой приз?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>После обмена хагов в Личном кабинете вам на почту придет письмо с ссылкой на выбранный вами подарок в интернет-магазине <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a> и персональным промокодом. Перейдите по ссылке из письма, положите ваш подарок в корзину в интернет-магазине <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a> и получите ваш подарок за 1 рубль. Обратите внимание, что ваш подарок вы можете получить только в случае заказа через интернет-магазин <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a>, в розничных магазинах на кассах промокоды не применяются. Доставка и самовывоз подарка из магазина осуществляется по правилам продажи указанными на сайте <a href="https://www.detmir.ru/catalog/index/name/sortforbrand/brand/2921/" target="_blank">Detmir.ru</a>.</p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Куда обратиться, если у меня есть вопросы или уточнения по программе?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Если у вас возникли вопросы по программе или нужна помощь обращайтесь в нашу службу поддержки через форму <a href="/feedback">Обратной связи</a> на сайте акции My.huggies.ru или пишите на email myhuggies@cleverbots.ru. Мы обязательно свяжемся с вами в течение 24 часов и ответим на все вопросы! </p>
                                </div>
                            </div>
                            <div class="faq_item">
                                <div class="faq_item_head">
                                    <div class="faq_item_btn js_faq_btn">
                                        <div class="faq_item_btn_inner">+</div>
                                    </div>
                                    <div class="faq_item_question">Какие документы нужны для подтверждения получения онлайн заказа?</div>
                                </div>
                                <div class="faq_item_info">
                                    <p>Для подтверждения получения онлайн заказа загрузите подтверждающий кассовый чек с qr-кодом, если заказ получен в магазине, чек с qr-кодом или фото накладной от службы доставки, если заказ доставлен курьерской службой или в точку самовывоза.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>