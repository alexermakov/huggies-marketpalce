<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>

            <?php include 'parts/myaccount/header.php'; ?>

            <?php include 'parts/myaccount/menu.php'; ?>

            <div class="myaccount__content">
                <div class="container">
                    <div class="myaccount__block myaccount__block__first">
                        <form action="">
                            <div class="myaccount__edit_info">
                                <div class="myaccount__block__title myaccount__block__title__2">Личные данные</div>
                                <div class="choose_gender_block choose_gender_block--small">
                                    <label class="js_choose_gender__item choose_gender__item choose_gender__item--woman active" data-gender='wooman'>
                                        <input checked="checked" name="Gender" type="radio" value="Female">
                                        <img src="images/icons/avatars/wooman.svg">
                                        <div class="choose_gender__title">Я - мама</div>
                                    </label>

                                    <div class="choose_gender__item choose_gender__item__view_block">
                                        <div class="choose_gender__item__view js_choose_gender__item__view">
                                            <div class="choose_gender__item__view__dot"></div>
                                        </div>
                                    </div>

                                    <label class="js_choose_gender__item choose_gender__item choose_gender__item--man" data-gender='man'>
                                        <input  name="Gender" type="radio" value="Male">
                                        <div class="choose_gender__title">Я - папа</div>
                                        <img src="images/icons/avatars/man.svg">
                                    </label>
                                </div>
                                <div class="field_row field_row--3">
                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Имя<span class="field_required">*</span>
                                            </div>
                                            <input name="Name" placeholder="Введите имя" type="text">
                                        </label>
                                    </div>
                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Фамилия<span class="field_required">*</span>
                                            </div>
                                            <input name="Surname" placeholder="Введите фамилию" type="text">
                                        </label>
                                    </div>
                                    <div class="field_item">
                                            <label class="field_item_label">
                                                <div class="field_item__title">
                                                    Город<span class="field_required">*</span>
                                                </div>
                                                <select class='js_select'>
                                                    <option value="490" data-select2-id="2">Москва</option>
                                                    <option value="781" data-select2-id="13">Санкт-Петербург</option>
                                                    <option value="634" data-select2-id="14">Новосибирск</option>
                                                    <option value="829" data-select2-id="15">Екатеринбург</option>
                                                    <option value="606" data-select2-id="16">Нижний Новгород</option>
                                                    <option value="922" data-select2-id="17">Казань</option>
                                                    <option value="1058" data-select2-id="18">Челябинск</option>
                                                    <option value="643" data-select2-id="19">Омск</option>
                                                    <option value="769" data-select2-id="20">Самара</option>
                                                    <option value="744" data-select2-id="21">Ростов-на-Дону</option>
                                                    <option value="62" data-select2-id="22">Уфа</option>
                                                    <option value="410" data-select2-id="23">Красноярск</option>
                                                    <option value="693" data-select2-id="24">Пермь</option>
                                                    <option value="159" data-select2-id="25">Воронеж</option>
                                                    <option value="121" data-select2-id="26">Волгоград</option>
                                                    <option value="1010" data-select2-id="27">Абаза</option>
                                                    <option value="1011" data-select2-id="28">Абакан</option>
                                                    <option value="646" data-select2-id="29">Абдулино</option>
                                                    <option value="370" data-select2-id="30">Абинск</option>
                                                    <option value="44" data-select2-id="31">Агидель</option>
                                                    <option value="911" data-select2-id="32">Агрыз</option>
                                                    <option value="1" data-select2-id="33">Адыгейск</option>
                                                    <option value="912" data-select2-id="34">Азнакаево</option>
                                                    <option value="727" data-select2-id="35">Азов</option>
                                                    <option value="981" data-select2-id="36">Ак-Довурак</option>
                                                    <option value="728" data-select2-id="37">Аксай</option>
                                                    <option value="863" data-select2-id="38">Алагир</option>
                                                    <option value="816" data-select2-id="39">Алапаевск</option>
                                                    <option value="1066" data-select2-id="40">Алатырь</option>
                                                    <option value="1078" data-select2-id="41">Алдан</option>
                                                    <option value="4" data-select2-id="42">Алейск</option>
                                                    <option value="98" data-select2-id="43">Александров</option>
                                                    <option value="676" data-select2-id="44">Александровск</option>
                                                    <option value="801" data-select2-id="45">Александровск-Сахалинский</option>
                                                    <option value="65" data-select2-id="46">Алексеевка</option>
                                                    <option value="962" data-select2-id="47">Алексин</option>
                                                    <option value="213" data-select2-id="48">Алзамай</option>
                                                    <option value="1124" data-select2-id="49">Алупка</option>
                                                    <option value="1123" data-select2-id="50">Алушта</option>
                                                    <option value="913" data-select2-id="51">Альметьевск</option>
                                                    <option value="1003" data-select2-id="52">Амурск</option>
                                                    <option value="1075" data-select2-id="53">Анадырь</option>
                                                    <option value="371" data-select2-id="54">Анапа</option>
                                                    <option value="214" data-select2-id="55">Ангарск</option>
                                                    <option value="933" data-select2-id="56">Андреаполь</option>
                                                    <option value="310" data-select2-id="57">Анжеро-Судженск</option>
                                                    <option value="802" data-select2-id="58">Анива</option>
                                                    <option value="571" data-select2-id="59">Апатиты</option>
                                                    <option value="494" data-select2-id="60">Апрелевка</option>
                                                    <option value="372" data-select2-id="61">Апшеронск</option>
                                                    <option value="817" data-select2-id="62">Арамиль</option>
                                                    <option value="1061" data-select2-id="63">Аргун</option>
                                                    <option value="483" data-select2-id="64">Ардатов</option>
                                                    <option value="864" data-select2-id="65">Ардон</option>
                                                    <option value="588" data-select2-id="66">Арзамас</option>
                                                    <option value="783" data-select2-id="67">Аркадак</option>
                                                    <option value="373" data-select2-id="68">Армавир</option>
                                                    <option value="1119" data-select2-id="69">Армянск</option>
                                                    <option value="701" data-select2-id="70">Арсеньев</option>
                                                    <option value="914" data-select2-id="71">Арск</option>
                                                    <option value="702" data-select2-id="72">Артём</option>
                                                    <option value="396" data-select2-id="73">Артёмовск</option>
                                                    <option value="818" data-select2-id="74">Артёмовский</option>
                                                    <option value="25" data-select2-id="75">Архангельск</option>
                                                    <option value="819" data-select2-id="76">Асбест</option>
                                                    <option value="956" data-select2-id="77">Асино</option>
                                                    <option value="38" data-select2-id="78">Астрахань</option>
                                                    <option value="784" data-select2-id="79">Аткарск</option>
                                                    <option value="39" data-select2-id="80">Ахтубинск</option>
                                                    <option value="397" data-select2-id="81">Ачинск</option>
                                                    <option value="1031" data-select2-id="82">Аша</option>
                                                    <option value="140" data-select2-id="83">Бабаево</option>
                                                    <option value="92" data-select2-id="84">Бабушкин</option>
                                                    <option value="915" data-select2-id="85">Бавлы</option>
                                                    <option value="243" data-select2-id="86">Багратионовск</option>
                                                    <option value="215" data-select2-id="87">Байкальск</option>
                                                    <option value="1135" data-select2-id="88">Байконур</option>
                                                    <option value="45" data-select2-id="89">Баймак</option>
                                                    <option value="1032" data-select2-id="90">Бакал</option>
                                                    <option value="235" data-select2-id="91">Баксан</option>
                                                    <option value="268" data-select2-id="92">Балабаново</option>
                                                    <option value="1112" data-select2-id="93">Балаклава</option>
                                                    <option value="785" data-select2-id="94">Балаково</option>
                                                    <option value="589" data-select2-id="95">Балахна</option>
                                                    <option value="495" data-select2-id="96">Балашиха</option>
                                                    <option value="786" data-select2-id="97">Балашов</option>
                                                    <option value="182" data-select2-id="98">Балей</option>
                                                    <option value="244" data-select2-id="99">Балтийск</option>
                                                    <option value="626" data-select2-id="100">Барабинск</option>
                                                    <option value="5" data-select2-id="101">Барнаул</option>
                                                    <option value="997" data-select2-id="102">Барыш</option>
                                                    <option value="729" data-select2-id="103">Батайск</option>
                                                    <option value="1113" data-select2-id="104">Бахчисарай</option>
                                                    <option value="934" data-select2-id="105">Бежецк</option>
                                                    <option value="730" data-select2-id="106">Белая Калитва</option>
                                                    <option value="330" data-select2-id="107">Белая Холуница</option>
                                                    <option value="66" data-select2-id="108">Белгород</option>
                                                    <option value="46" data-select2-id="109">Белебей</option>
                                                    <option value="963" data-select2-id="110">Белёв</option>
                                                    <option value="665" data-select2-id="111">Белинский</option>
                                                    <option value="311" data-select2-id="112">Белово</option>
                                                    <option value="1121" data-select2-id="113">Белогорск</option>
                                                    <option value="16" data-select2-id="114">Белогорск</option>
                                                    <option value="141" data-select2-id="115">Белозерск</option>
                                                    <option value="6" data-select2-id="116">Белокуриха</option>
                                                    <option value="297" data-select2-id="117">Беломорск</option>
                                                    <option value="47" data-select2-id="118">Белорецк</option>
                                                    <option value="374" data-select2-id="119">Белореченск</option>
                                                    <option value="269" data-select2-id="120">Белоусово</option>
                                                    <option value="1015" data-select2-id="121">Белоярский</option>
                                                    <option value="935" data-select2-id="122">Белый</option>
                                                    <option value="627" data-select2-id="123">Бердск</option>
                                                    <option value="677" data-select2-id="124">Березники</option>
                                                    <option value="312" data-select2-id="125">Берёзовский (Кемеровская область)</option>
                                                    <option value="820" data-select2-id="126">Берёзовский (Свердловская область)</option>
                                                    <option value="865" data-select2-id="127">Беслан</option>
                                                    <option value="7" data-select2-id="128">Бийск</option>
                                                    <option value="1004" data-select2-id="129">Бикин</option>
                                                    <option value="1076" data-select2-id="130">Билибино</option>
                                                    <option value="180" data-select2-id="131">Биробиджан</option>
                                                    <option value="48" data-select2-id="132">Бирск</option>
                                                    <option value="216" data-select2-id="133">Бирюсинск</option>
                                                    <option value="67" data-select2-id="134">Бирюч</option>
                                                    <option value="17" data-select2-id="135">Благовещенск (Амурская область)</option>
                                                    <option value="49" data-select2-id="136">Благовещенск (Республика Башкортостан)</option>
                                                    <option value="884" data-select2-id="137">Благодарный</option>
                                                    <option value="155" data-select2-id="138">Бобров</option>
                                                    <option value="821" data-select2-id="139">Богданович</option>
                                                    <option value="964" data-select2-id="140">Богородицк</option>
                                                    <option value="590" data-select2-id="141">Богородск</option>
                                                    <option value="398" data-select2-id="142">Боготол</option>
                                                    <option value="156" data-select2-id="143">Богучар</option>
                                                    <option value="217" data-select2-id="144">Бодайбо</option>
                                                    <option value="438" data-select2-id="145">Бокситогорск</option>
                                                    <option value="916" data-select2-id="146">Болгар</option>
                                                    <option value="936" data-select2-id="147">Бологое</option>
                                                    <option value="628" data-select2-id="148">Болотное</option>
                                                    <option value="965" data-select2-id="149">Болохово</option>
                                                    <option value="658" data-select2-id="150">Болхов</option>
                                                    <option value="703" data-select2-id="151">Большой Камень</option>
                                                    <option value="591" data-select2-id="152">Бор</option>
                                                    <option value="183" data-select2-id="153">Борзя</option>
                                                    <option value="157" data-select2-id="154">Борисоглебск</option>
                                                    <option value="616" data-select2-id="155">Боровичи</option>
                                                    <option value="270" data-select2-id="156">Боровск</option>
                                                    <option value="399" data-select2-id="157">Бородино</option>
                                                    <option value="218" data-select2-id="158">Братск</option>
                                                    <option value="496" data-select2-id="159">Бронницы</option>
                                                    <option value="76" data-select2-id="160">Брянск</option>
                                                    <option value="917" data-select2-id="161">Бугульма</option>
                                                    <option value="647" data-select2-id="162">Бугуруслан</option>
                                                    <option value="885" data-select2-id="163">Будённовск</option>
                                                    <option value="648" data-select2-id="164">Бузулук</option>
                                                    <option value="358" data-select2-id="165">Буй</option>
                                                    <option value="170" data-select2-id="166">Буйнакск</option>
                                                    <option value="918" data-select2-id="167">Буинск</option>
                                                    <option value="158" data-select2-id="168">Бутурлиновка</option>
                                                    <option value="617" data-select2-id="169">Валдай</option>
                                                    <option value="68" data-select2-id="170">Валуйки</option>
                                                    <option value="869" data-select2-id="171">Велиж</option>
                                                    <option value="713" data-select2-id="172">Великие Луки</option>
                                                    <option value="618" data-select2-id="173">Великий Новгород</option>
                                                    <option value="142" data-select2-id="174">Великий Устюг</option>
                                                    <option value="26" data-select2-id="175">Вельск</option>
                                                    <option value="966" data-select2-id="176">Венёв</option>
                                                    <option value="678" data-select2-id="177">Верещагино</option>
                                                    <option value="497" data-select2-id="178">Верея</option>
                                                    <option value="1033" data-select2-id="179">Верхнеуральск</option>
                                                    <option value="822" data-select2-id="180">Верхний Тагил</option>
                                                    <option value="1034" data-select2-id="181">Верхний Уфалей</option>
                                                    <option value="823" data-select2-id="182">Верхняя Пышма</option>
                                                    <option value="824" data-select2-id="183">Верхняя Салда</option>
                                                    <option value="825" data-select2-id="184">Верхняя Тура</option>
                                                    <option value="826" data-select2-id="185">Верхотурье</option>
                                                    <option value="1079" data-select2-id="186">Верхоянск</option>
                                                    <option value="937" data-select2-id="187">Весьегонск</option>
                                                    <option value="592" data-select2-id="188">Ветлуга</option>
                                                    <option value="498" data-select2-id="189">Видное</option>
                                                    <option value="1080" data-select2-id="190">Вилюйск</option>
                                                    <option value="290" data-select2-id="191">Вилючинск</option>
                                                    <option value="219" data-select2-id="192">Вихоревка</option>
                                                    <option value="192" data-select2-id="193">Вичуга</option>
                                                    <option value="704" data-select2-id="194">Владивосток</option>
                                                    <option value="866" data-select2-id="195">Владикавказ</option>
                                                    <option value="99" data-select2-id="196">Владимир</option>
                                                    <option value="731" data-select2-id="197">Волгодонск</option>
                                                    <option value="359" data-select2-id="198">Волгореченск</option>
                                                    <option value="479" data-select2-id="199">Волжск</option>
                                                    <option value="122" data-select2-id="200">Волжский</option>
                                                    <option value="143" data-select2-id="201">Вологда</option>
                                                    <option value="593" data-select2-id="202">Володарск</option>
                                                    <option value="499" data-select2-id="203">Волоколамск</option>
                                                    <option value="439" data-select2-id="204">Волосово</option>
                                                    <option value="440" data-select2-id="205">Волхов</option>
                                                    <option value="827" data-select2-id="206">Волчанск</option>
                                                    <option value="787" data-select2-id="207">Вольск</option>
                                                    <option value="348" data-select2-id="208">Воркута</option>
                                                    <option value="594" data-select2-id="209">Ворсма</option>
                                                    <option value="500" data-select2-id="210">Воскресенск</option>
                                                    <option value="991" data-select2-id="211">Воткинск</option>
                                                    <option value="441" data-select2-id="212">Всеволожск</option>
                                                    <option value="349" data-select2-id="213">Вуктыл</option>
                                                    <option value="442" data-select2-id="214">Выборг</option>
                                                    <option value="595" data-select2-id="215">Выкса</option>
                                                    <option value="501" data-select2-id="216">Высоковск</option>
                                                    <option value="443" data-select2-id="217">Высоцк</option>
                                                    <option value="144" data-select2-id="218">Вытегра</option>
                                                    <option value="938" data-select2-id="219">Вышний Волочёк</option>
                                                    <option value="1005" data-select2-id="220">Вяземский</option>
                                                    <option value="100" data-select2-id="221">Вязники</option>
                                                    <option value="870" data-select2-id="222">Вязьма</option>
                                                    <option value="331" data-select2-id="223">Вятские Поляны</option>
                                                    <option value="1099" data-select2-id="224">Гаврилов-Ям</option>
                                                    <option value="193" data-select2-id="225">Гаврилов Посад</option>
                                                    <option value="871" data-select2-id="226">Гагарин</option>
                                                    <option value="572" data-select2-id="227">Гаджиево</option>
                                                    <option value="649" data-select2-id="228">Гай</option>
                                                    <option value="360" data-select2-id="229">Галич</option>
                                                    <option value="444" data-select2-id="230">Гатчина</option>
                                                    <option value="245" data-select2-id="231">Гвардейск</option>
                                                    <option value="714" data-select2-id="232">Гдов</option>
                                                    <option value="375" data-select2-id="233">Геленджик</option>
                                                    <option value="886" data-select2-id="234">Георгиевск</option>
                                                    <option value="992" data-select2-id="235">Глазов</option>
                                                    <option value="502" data-select2-id="236">Голицыно</option>
                                                    <option value="596" data-select2-id="237">Горбатов</option>
                                                    <option value="3" data-select2-id="238">Горно-Алтайск</option>
                                                    <option value="679" data-select2-id="239">Горнозаводск</option>
                                                    <option value="8" data-select2-id="240">Горняк</option>
                                                    <option value="597" data-select2-id="241">Городец</option>
                                                    <option value="666" data-select2-id="242">Городище</option>
                                                    <option value="265" data-select2-id="243">Городовиковск</option>
                                                    <option value="101" data-select2-id="244">Гороховец</option>
                                                    <option value="376" data-select2-id="245">Горячий Ключ</option>
                                                    <option value="69" data-select2-id="246">Грайворон</option>
                                                    <option value="680" data-select2-id="247">Гремячинск</option>
                                                    <option value="1062" data-select2-id="248">Грозный</option>
                                                    <option value="469" data-select2-id="249">Грязи</option>
                                                    <option value="145" data-select2-id="250">Грязовец</option>
                                                    <option value="681" data-select2-id="251">Губаха</option>
                                                    <option value="70" data-select2-id="252">Губкин</option>
                                                    <option value="1091" data-select2-id="253">Губкинский</option>
                                                    <option value="1063" data-select2-id="254">Гудермес</option>
                                                    <option value="732" data-select2-id="255">Гуково</option>
                                                    <option value="377" data-select2-id="256">Гулькевичи</option>
                                                    <option value="1126" data-select2-id="257">Гурзуф</option>
                                                    <option value="246" data-select2-id="258">Гурьевск (Калининградская область)</option>
                                                    <option value="313" data-select2-id="259">Гурьевск (Кемеровская область)</option>
                                                    <option value="247" data-select2-id="260">Гусев</option>
                                                    <option value="93" data-select2-id="261">Гусиноозёрск</option>
                                                    <option value="102" data-select2-id="262">Гусь-Хрустальный</option>
                                                    <option value="50" data-select2-id="263">Давлеканово</option>
                                                    <option value="171" data-select2-id="264">Дагестанские Огни</option>
                                                    <option value="419" data-select2-id="265">Далматово</option>
                                                    <option value="705" data-select2-id="266">Дальнегорск</option>
                                                    <option value="706" data-select2-id="267">Дальнереченск</option>
                                                    <option value="1100" data-select2-id="268">Данилов</option>
                                                    <option value="470" data-select2-id="269">Данков</option>
                                                    <option value="828" data-select2-id="270">Дегтярск</option>
                                                    <option value="503" data-select2-id="271">Дедовск</option>
                                                    <option value="872" data-select2-id="272">Демидов</option>
                                                    <option value="172" data-select2-id="273">Дербент</option>
                                                    <option value="873" data-select2-id="274">Десногорск</option>
                                                    <option value="1117" data-select2-id="275">Джанкой</option>
                                                    <option value="598" data-select2-id="276">Дзержинск</option>
                                                    <option value="504" data-select2-id="277">Дзержинский</option>
                                                    <option value="400" data-select2-id="278">Дивногорск</option>
                                                    <option value="867" data-select2-id="279">Дигора</option>
                                                    <option value="998" data-select2-id="280">Димитровград</option>
                                                    <option value="428" data-select2-id="281">Дмитриев</option>
                                                    <option value="505" data-select2-id="282">Дмитров</option>
                                                    <option value="659" data-select2-id="283">Дмитровск</option>
                                                    <option value="715" data-select2-id="284">Дно</option>
                                                    <option value="682" data-select2-id="285">Добрянка</option>
                                                    <option value="506" data-select2-id="286">Долгопрудный</option>
                                                    <option value="803" data-select2-id="287">Долинск</option>
                                                    <option value="507" data-select2-id="288">Домодедово</option>
                                                    <option value="733" data-select2-id="289">Донецк</option>
                                                    <option value="967" data-select2-id="290">Донской</option>
                                                    <option value="874" data-select2-id="291">Дорогобуж</option>
                                                    <option value="508" data-select2-id="292">Дрезна</option>
                                                    <option value="509" data-select2-id="293">Дубна</option>
                                                    <option value="123" data-select2-id="294">Дубовка</option>
                                                    <option value="401" data-select2-id="295">Дудинка</option>
                                                    <option value="875" data-select2-id="296">Духовщина</option>
                                                    <option value="51" data-select2-id="297">Дюртюли</option>
                                                    <option value="77" data-select2-id="298">Дятьково</option>
                                                    <option value="1114" data-select2-id="299">Евпатория</option>
                                                    <option value="510" data-select2-id="300">Егорьевск</option>
                                                    <option value="378" data-select2-id="301">Ейск</option>
                                                    <option value="919" data-select2-id="302">Елабуга</option>
                                                    <option value="471" data-select2-id="303">Елец</option>
                                                    <option value="291" data-select2-id="304">Елизово</option>
                                                    <option value="876" data-select2-id="305">Ельня</option>
                                                    <option value="1035" data-select2-id="306">Еманжелинск</option>
                                                    <option value="350" data-select2-id="307">Емва</option>
                                                    <option value="402" data-select2-id="308">Енисейск</option>
                                                    <option value="271" data-select2-id="309">Ермолино</option>
                                                    <option value="788" data-select2-id="310">Ершов</option>
                                                    <option value="887" data-select2-id="311">Ессентуки</option>
                                                    <option value="968" data-select2-id="312">Ефремов</option>
                                                    <option value="888" data-select2-id="313">Железноводск</option>
                                                    <option value="220" data-select2-id="314">Железногорск-Илимский</option>
                                                    <option value="403" data-select2-id="315">Железногорск (Красноярский край)</option>
                                                    <option value="429" data-select2-id="316">Железногорск (Курская область)</option>
                                                    <option value="511" data-select2-id="317">Железнодорожный</option>
                                                    <option value="903" data-select2-id="318">Жердевка</option>
                                                    <option value="762" data-select2-id="319">Жигулёвск</option>
                                                    <option value="272" data-select2-id="320">Жиздра</option>
                                                    <option value="124" data-select2-id="321">Жирновск</option>
                                                    <option value="273" data-select2-id="322">Жуков</option>
                                                    <option value="78" data-select2-id="323">Жуковка</option>
                                                    <option value="512" data-select2-id="324">Жуковский</option>
                                                    <option value="18" data-select2-id="325">Завитинск</option>
                                                    <option value="986" data-select2-id="326">Заводоуковск</option>
                                                    <option value="194" data-select2-id="327">Заволжск</option>
                                                    <option value="599" data-select2-id="328">Заволжье</option>
                                                    <option value="472" data-select2-id="329">Задонск</option>
                                                    <option value="920" data-select2-id="330">Заинск</option>
                                                    <option value="94" data-select2-id="331">Закаменск</option>
                                                    <option value="404" data-select2-id="332">Заозёрный</option>
                                                    <option value="573" data-select2-id="333">Заозёрск</option>
                                                    <option value="939" data-select2-id="334">Западная Двина</option>
                                                    <option value="574" data-select2-id="335">Заполярный</option>
                                                    <option value="513" data-select2-id="336">Зарайск</option>
                                                    <option value="667" data-select2-id="337">Заречный (Пензенская область)</option>
                                                    <option value="830" data-select2-id="338">Заречный (Свердловская область)</option>
                                                    <option value="9" data-select2-id="339">Заринск</option>
                                                    <option value="480" data-select2-id="340">Звенигово</option>
                                                    <option value="514" data-select2-id="341">Звенигород</option>
                                                    <option value="734" data-select2-id="342">Зверево</option>
                                                    <option value="405" data-select2-id="343">Зеленогорск (Красноярский край)</option>
                                                    <option value="773" data-select2-id="344">Зеленогорск (Санкт-Петербург)</option>
                                                    <option value="248" data-select2-id="345">Зеленоградск</option>
                                                    <option value="921" data-select2-id="346">Зеленодольск</option>
                                                    <option value="889" data-select2-id="347">Зеленокумск</option>
                                                    <option value="735" data-select2-id="348">Зерноград</option>
                                                    <option value="19" data-select2-id="349">Зея</option>
                                                    <option value="221" data-select2-id="350">Зима</option>
                                                    <option value="1036" data-select2-id="351">Златоуст</option>
                                                    <option value="79" data-select2-id="352">Злынка</option>
                                                    <option value="10" data-select2-id="353">Змеиногорск</option>
                                                    <option value="40" data-select2-id="354">Знаменск</option>
                                                    <option value="940" data-select2-id="355">Зубцов</option>
                                                    <option value="332" data-select2-id="356">Зуевка</option>
                                                    <option value="445" data-select2-id="357">Ивангород</option>
                                                    <option value="195" data-select2-id="358">Иваново</option>
                                                    <option value="515" data-select2-id="359">Ивантеевка</option>
                                                    <option value="831" data-select2-id="360">Ивдель</option>
                                                    <option value="406" data-select2-id="361">Игарка</option>
                                                    <option value="993" data-select2-id="362">Ижевск</option>
                                                    <option value="173" data-select2-id="363">Избербаш</option>
                                                    <option value="890" data-select2-id="364">Изобильный</option>
                                                    <option value="407" data-select2-id="365">Иланский</option>
                                                    <option value="999" data-select2-id="366">Инза</option>
                                                    <option value="1111" data-select2-id="367">Инкерман</option>
                                                    <option value="484" data-select2-id="368">Инсар</option>
                                                    <option value="351" data-select2-id="369">Инта</option>
                                                    <option value="481" data-select2-id="370">Йошкар-Ола</option>
                                                    <option value="891" data-select2-id="371">Ипатово</option>
                                                    <option value="832" data-select2-id="372">Ирбит</option>
                                                    <option value="222" data-select2-id="373">Иркутск</option>
                                                    <option value="640" data-select2-id="374">Исилькуль</option>
                                                    <option value="629" data-select2-id="375">Искитим</option>
                                                    <option value="516" data-select2-id="376">Истра</option>
                                                    <option value="987" data-select2-id="377">Ишим</option>
                                                    <option value="52" data-select2-id="378">Ишимбай</option>
                                                    <option value="146" data-select2-id="379">Кадников</option>
                                                    <option value="160" data-select2-id="380">Калач</option>
                                                    <option value="125" data-select2-id="381">Калач-на-Дону</option>
                                                    <option value="641" data-select2-id="382">Калачинск</option>
                                                    <option value="249" data-select2-id="383">Калининград</option>
                                                    <option value="789" data-select2-id="384">Калининск</option>
                                                    <option value="314" data-select2-id="385">Калтан</option>
                                                    <option value="274" data-select2-id="386">Калуга</option>
                                                    <option value="941" data-select2-id="387">Калязин</option>
                                                    <option value="994" data-select2-id="388">Камбарка</option>
                                                    <option value="668" data-select2-id="389">Каменка</option>
                                                    <option value="446" data-select2-id="390">Каменногорск</option>
                                                    <option value="833" data-select2-id="391">Каменск-Уральский</option>
                                                    <option value="736" data-select2-id="392">Каменск-Шахтинский</option>
                                                    <option value="11" data-select2-id="393">Камень-на-Оби</option>
                                                    <option value="103" data-select2-id="394">Камешково</option>
                                                    <option value="41" data-select2-id="395">Камызяк</option>
                                                    <option value="126" data-select2-id="396">Камышин</option>
                                                    <option value="834" data-select2-id="397">Камышлов</option>
                                                    <option value="1067" data-select2-id="398">Канаш</option>
                                                    <option value="575" data-select2-id="399">Кандалакша</option>
                                                    <option value="408" data-select2-id="400">Канск</option>
                                                    <option value="104" data-select2-id="401">Карабаново</option>
                                                    <option value="1037" data-select2-id="402">Карабаш</option>
                                                    <option value="209" data-select2-id="403">Карабулак</option>
                                                    <option value="630" data-select2-id="404">Карасук</option>
                                                    <option value="293" data-select2-id="405">Карачаевск</option>
                                                    <option value="80" data-select2-id="406">Карачев</option>
                                                    <option value="631" data-select2-id="407">Каргат</option>
                                                    <option value="27" data-select2-id="408">Каргополь</option>
                                                    <option value="835" data-select2-id="409">Карпинск</option>
                                                    <option value="1038" data-select2-id="410">Карталы</option>
                                                    <option value="750" data-select2-id="411">Касимов</option>
                                                    <option value="1039" data-select2-id="412">Касли</option>
                                                    <option value="174" data-select2-id="413">Каспийск</option>
                                                    <option value="1040" data-select2-id="414">Катав-Ивановск</option>
                                                    <option value="420" data-select2-id="415">Катайск</option>
                                                    <option value="836" data-select2-id="416">Качканар</option>
                                                    <option value="942" data-select2-id="417">Кашин</option>
                                                    <option value="517" data-select2-id="418">Кашира</option>
                                                    <option value="957" data-select2-id="419">Кедровый</option>
                                                    <option value="315" data-select2-id="420">Кемерово</option>
                                                    <option value="298" data-select2-id="421">Кемь</option>
                                                    <option value="1133" data-select2-id="422">Керчь</option>
                                                    <option value="683" data-select2-id="423">Кизел</option>
                                                    <option value="175" data-select2-id="424">Кизилюрт</option>
                                                    <option value="176" data-select2-id="425">Кизляр</option>
                                                    <option value="969" data-select2-id="426">Кимовск</option>
                                                    <option value="943" data-select2-id="427">Кимры</option>
                                                    <option value="447" data-select2-id="428">Кингисепп</option>
                                                    <option value="763" data-select2-id="429">Кинель</option>
                                                    <option value="196" data-select2-id="430">Кинешма</option>
                                                    <option value="970" data-select2-id="431">Киреевск</option>
                                                    <option value="223" data-select2-id="432">Киренск</option>
                                                    <option value="105" data-select2-id="433">Киржач</option>
                                                    <option value="147" data-select2-id="434">Кириллов</option>
                                                    <option value="448" data-select2-id="435">Кириши</option>
                                                    <option value="275" data-select2-id="436">Киров (Калужская область)</option>
                                                    <option value="333" data-select2-id="437">Киров (Кировская область)</option>
                                                    <option value="837" data-select2-id="438">Кировград</option>
                                                    <option value="334" data-select2-id="439">Кирово-Чепецк</option>
                                                    <option value="449" data-select2-id="440">Кировск (Ленинградская область)</option>
                                                    <option value="576" data-select2-id="441">Кировск (Мурманская область)</option>
                                                    <option value="335" data-select2-id="442">Кирс</option>
                                                    <option value="904" data-select2-id="443">Кирсанов</option>
                                                    <option value="316" data-select2-id="444">Киселёвск</option>
                                                    <option value="892" data-select2-id="445">Кисловодск</option>
                                                    <option value="518" data-select2-id="446">Климовск</option>
                                                    <option value="519" data-select2-id="447">Клин</option>
                                                    <option value="81" data-select2-id="448">Клинцы</option>
                                                    <option value="600" data-select2-id="449">Княгинино</option>
                                                    <option value="577" data-select2-id="450">Ковдор</option>
                                                    <option value="106" data-select2-id="451">Ковров</option>
                                                    <option value="485" data-select2-id="452">Ковылкино</option>
                                                    <option value="1016" data-select2-id="453">Когалым</option>
                                                    <option value="409" data-select2-id="454">Кодинск</option>
                                                    <option value="276" data-select2-id="455">Козельск</option>
                                                    <option value="1068" data-select2-id="456">Козловка</option>
                                                    <option value="482" data-select2-id="457">Козьмодемьянск</option>
                                                    <option value="1132" data-select2-id="458">Коктебель</option>
                                                    <option value="578" data-select2-id="459">Кола</option>
                                                    <option value="361" data-select2-id="460">Кологрив</option>
                                                    <option value="520" data-select2-id="461">Коломна</option>
                                                    <option value="958" data-select2-id="462">Колпашево</option>
                                                    <option value="774" data-select2-id="463">Колпино</option>
                                                    <option value="107" data-select2-id="464">Кольчугино</option>
                                                    <option value="450" data-select2-id="465">Коммунар</option>
                                                    <option value="197" data-select2-id="466">Комсомольск</option>
                                                    <option value="1006" data-select2-id="467">Комсомольск-на-Амуре</option>
                                                    <option value="944" data-select2-id="468">Конаково</option>
                                                    <option value="299" data-select2-id="469">Кондопога</option>
                                                    <option value="277" data-select2-id="470">Кондрово</option>
                                                    <option value="737" data-select2-id="471">Константиновск</option>
                                                    <option value="1041" data-select2-id="472">Копейск</option>
                                                    <option value="751" data-select2-id="473">Кораблино</option>
                                                    <option value="379" data-select2-id="474">Кореновск</option>
                                                    <option value="1042" data-select2-id="475">Коркино</option>
                                                    <option value="522" data-select2-id="476">Королёв</option>
                                                    <option value="71" data-select2-id="477">Короча</option>
                                                    <option value="804" data-select2-id="478">Корсаков</option>
                                                    <option value="28" data-select2-id="479">Коряжма</option>
                                                    <option value="108" data-select2-id="480">Костерёво</option>
                                                    <option value="300" data-select2-id="481">Костомукша</option>
                                                    <option value="362" data-select2-id="482">Кострома</option>
                                                    <option value="521" data-select2-id="483">Котельники</option>
                                                    <option value="127" data-select2-id="484">Котельниково</option>
                                                    <option value="336" data-select2-id="485">Котельнич</option>
                                                    <option value="29" data-select2-id="486">Котлас</option>
                                                    <option value="128" data-select2-id="487">Котово</option>
                                                    <option value="905" data-select2-id="488">Котовск</option>
                                                    <option value="198" data-select2-id="489">Кохма</option>
                                                    <option value="148" data-select2-id="490">Красавино</option>
                                                    <option value="523" data-select2-id="491">Красноармейск (Московская область)</option>
                                                    <option value="790" data-select2-id="492">Красноармейск (Саратовская область)</option>
                                                    <option value="684" data-select2-id="493">Красновишерск</option>
                                                    <option value="524" data-select2-id="494">Красногорск</option>
                                                    <option value="380" data-select2-id="495">Краснодар</option>
                                                    <option value="775" data-select2-id="496">Красное Село</option>
                                                    <option value="525" data-select2-id="497">Краснозаводск</option>
                                                    <option value="250" data-select2-id="498">Краснознаменск (Калининградская область)</option>
                                                    <option value="526" data-select2-id="499">Краснознаменск (Московская область)</option>
                                                    <option value="184" data-select2-id="500">Краснокаменск</option>
                                                    <option value="685" data-select2-id="501">Краснокамск</option>
                                                    <option value="1118" data-select2-id="502">Красноперекопск</option>
                                                    <option value="129" data-select2-id="503">Краснослободск (Волгоградская область)</option>
                                                    <option value="486" data-select2-id="504">Краснослободск (Мордовия)</option>
                                                    <option value="838" data-select2-id="505">Краснотурьинск</option>
                                                    <option value="839" data-select2-id="506">Красноуральск</option>
                                                    <option value="840" data-select2-id="507">Красноуфимск</option>
                                                    <option value="791" data-select2-id="508">Красный Кут</option>
                                                    <option value="738" data-select2-id="509">Красный Сулин</option>
                                                    <option value="945" data-select2-id="510">Красный Холм</option>
                                                    <option value="278" data-select2-id="511">Кремёнки</option>
                                                    <option value="776" data-select2-id="512">Кронштадт</option>
                                                    <option value="381" data-select2-id="513">Кропоткин</option>
                                                    <option value="382" data-select2-id="514">Крымск</option>
                                                    <option value="601" data-select2-id="515">Кстово</option>
                                                    <option value="527" data-select2-id="516">Кубинка</option>
                                                    <option value="650" data-select2-id="517">Кувандык</option>
                                                    <option value="946" data-select2-id="518">Кувшиново</option>
                                                    <option value="686" data-select2-id="519">Кудымкар</option>
                                                    <option value="669" data-select2-id="520">Кузнецк</option>
                                                    <option value="632" data-select2-id="521">Куйбышев</option>
                                                    <option value="602" data-select2-id="522">Кулебаки</option>
                                                    <option value="53" data-select2-id="523">Кумертау</option>
                                                    <option value="687" data-select2-id="524">Кунгур</option>
                                                    <option value="633" data-select2-id="525">Купино</option>
                                                    <option value="421" data-select2-id="526">Курган</option>
                                                    <option value="383" data-select2-id="527">Курганинск</option>
                                                    <option value="805" data-select2-id="528">Курильск</option>
                                                    <option value="109" data-select2-id="529">Курлово</option>
                                                    <option value="528" data-select2-id="530">Куровское</option>
                                                    <option value="430" data-select2-id="531">Курск</option>
                                                    <option value="422" data-select2-id="532">Куртамыш</option>
                                                    <option value="431" data-select2-id="533">Курчатов</option>
                                                    <option value="1043" data-select2-id="534">Куса</option>
                                                    <option value="841" data-select2-id="535">Кушва</option>
                                                    <option value="982" data-select2-id="536">Кызыл</option>
                                                    <option value="1044" data-select2-id="537">Кыштым</option>
                                                    <option value="95" data-select2-id="538">Кяхта</option>
                                                    <option value="384" data-select2-id="539">Лабинск</option>
                                                    <option value="1092" data-select2-id="540">Лабытнанги</option>
                                                    <option value="266" data-select2-id="541">Лагань</option>
                                                    <option value="251" data-select2-id="542">Ладушкин</option>
                                                    <option value="923" data-select2-id="543">Лаишево</option>
                                                    <option value="110" data-select2-id="544">Лакинск</option>
                                                    <option value="1017" data-select2-id="545">Лангепас</option>
                                                    <option value="301" data-select2-id="546">Лахденпохья</option>
                                                    <option value="473" data-select2-id="547">Лебедянь</option>
                                                    <option value="924" data-select2-id="548">Лениногорск</option>
                                                    <option value="130" data-select2-id="549">Ленинск</option>
                                                    <option value="317" data-select2-id="550">Ленинск-Кузнецкий</option>
                                                    <option value="1081" data-select2-id="551">Ленск</option>
                                                    <option value="893" data-select2-id="552">Лермонтов</option>
                                                    <option value="842" data-select2-id="553">Лесной</option>
                                                    <option value="707" data-select2-id="554">Лесозаводск</option>
                                                    <option value="411" data-select2-id="555">Лесосибирск</option>
                                                    <option value="660" data-select2-id="556">Ливны</option>
                                                    <option value="529" data-select2-id="557">Ликино-Дулёво</option>
                                                    <option value="474" data-select2-id="558">Липецк</option>
                                                    <option value="971" data-select2-id="559">Липки</option>
                                                    <option value="161" data-select2-id="560">Лиски</option>
                                                    <option value="947" data-select2-id="561">Лихославль</option>
                                                    <option value="530" data-select2-id="562">Лобня</option>
                                                    <option value="451" data-select2-id="563">Лодейное Поле</option>
                                                    <option value="777" data-select2-id="564">Ломоносов</option>
                                                    <option value="531" data-select2-id="565">Лосино-Петровский</option>
                                                    <option value="452" data-select2-id="566">Луга</option>
                                                    <option value="337" data-select2-id="567">Луза</option>
                                                    <option value="603" data-select2-id="568">Лукоянов</option>
                                                    <option value="532" data-select2-id="569">Луховицы</option>
                                                    <option value="604" data-select2-id="570">Лысково</option>
                                                    <option value="688" data-select2-id="571">Лысьва</option>
                                                    <option value="533" data-select2-id="572">Лыткарино</option>
                                                    <option value="432" data-select2-id="573">Льгов</option>
                                                    <option value="453" data-select2-id="574">Любань</option>
                                                    <option value="534" data-select2-id="575">Люберцы</option>
                                                    <option value="1101" data-select2-id="576">Любим</option>
                                                    <option value="279" data-select2-id="577">Людиново</option>
                                                    <option value="1018" data-select2-id="578">Лянтор</option>
                                                    <option value="477" data-select2-id="579">Магадан</option>
                                                    <option value="210" data-select2-id="580">Магас</option>
                                                    <option value="1045" data-select2-id="581">Магнитогорск</option>
                                                    <option value="2" data-select2-id="582">Майкоп</option>
                                                    <option value="236" data-select2-id="583">Майский</option>
                                                    <option value="806" data-select2-id="584">Макаров</option>
                                                    <option value="363" data-select2-id="585">Макарьев</option>
                                                    <option value="423" data-select2-id="586">Макушино</option>
                                                    <option value="619" data-select2-id="587">Малая Вишера</option>
                                                    <option value="211" data-select2-id="588">Малгобек</option>
                                                    <option value="338" data-select2-id="589">Малмыж</option>
                                                    <option value="661" data-select2-id="590">Малоархангельск</option>
                                                    <option value="280" data-select2-id="591">Малоярославец</option>
                                                    <option value="925" data-select2-id="592">Мамадыш</option>
                                                    <option value="252" data-select2-id="593">Мамоново</option>
                                                    <option value="364" data-select2-id="594">Мантурово</option>
                                                    <option value="318" data-select2-id="595">Мариинск</option>
                                                    <option value="1069" data-select2-id="596">Мариинский Посад</option>
                                                    <option value="792" data-select2-id="597">Маркс</option>
                                                    <option value="177" data-select2-id="598">Махачкала</option>
                                                    <option value="82" data-select2-id="599">Мглин</option>
                                                    <option value="1019" data-select2-id="600">Мегион</option>
                                                    <option value="302" data-select2-id="601">Медвежьегорск</option>
                                                    <option value="651" data-select2-id="602">Медногорск</option>
                                                    <option value="281" data-select2-id="603">Медынь</option>
                                                    <option value="54" data-select2-id="604">Межгорье</option>
                                                    <option value="319" data-select2-id="605">Междуреченск</option>
                                                    <option value="30" data-select2-id="606">Мезень</option>
                                                    <option value="111" data-select2-id="607">Меленки</option>
                                                    <option value="55" data-select2-id="608">Мелеуз</option>
                                                    <option value="926" data-select2-id="609">Менделеевск</option>
                                                    <option value="927" data-select2-id="610">Мензелинск</option>
                                                    <option value="282" data-select2-id="611">Мещовск</option>
                                                    <option value="1046" data-select2-id="612">Миасс</option>
                                                    <option value="352" data-select2-id="613">Микунь</option>
                                                    <option value="739" data-select2-id="614">Миллерово</option>
                                                    <option value="894" data-select2-id="615">Минеральные Воды</option>
                                                    <option value="412" data-select2-id="616">Минусинск</option>
                                                    <option value="1047" data-select2-id="617">Миньяр</option>
                                                    <option value="31" data-select2-id="618">Мирный (Архангельская область)</option>
                                                    <option value="1082" data-select2-id="619">Мирный (Якутия)</option>
                                                    <option value="752" data-select2-id="620">Михайлов</option>
                                                    <option value="131" data-select2-id="621">Михайловка</option>
                                                    <option value="843" data-select2-id="622">Михайловск (Свердловская область)</option>
                                                    <option value="895" data-select2-id="623">Михайловск (Ставропольский край)</option>
                                                    <option value="906" data-select2-id="624">Мичуринск</option>
                                                    <option value="185" data-select2-id="625">Могоча</option>
                                                    <option value="535" data-select2-id="626">Можайск</option>
                                                    <option value="995" data-select2-id="627">Можга</option>
                                                    <option value="868" data-select2-id="628">Моздок</option>
                                                    <option value="579" data-select2-id="629">Мончегорск</option>
                                                    <option value="740" data-select2-id="630">Морозовск</option>
                                                    <option value="907" data-select2-id="631">Моршанск</option>
                                                    <option value="283" data-select2-id="632">Мосальск</option>
                                                    <option value="491" data-select2-id="633">Московский</option>
                                                    <option value="1093" data-select2-id="634">Муравленко</option>
                                                    <option value="339" data-select2-id="635">Мураши</option>
                                                    <option value="580" data-select2-id="636">Мурманск</option>
                                                    <option value="112" data-select2-id="637">Муром</option>
                                                    <option value="662" data-select2-id="638">Мценск</option>
                                                    <option value="320" data-select2-id="639">Мыски</option>
                                                    <option value="536" data-select2-id="640">Мытищи</option>
                                                    <option value="1102" data-select2-id="641">Мышкин</option>
                                                    <option value="928" data-select2-id="642">Набережные Челны</option>
                                                    <option value="605" data-select2-id="643">Навашино</option>
                                                    <option value="199" data-select2-id="644">Наволоки</option>
                                                    <option value="1094" data-select2-id="645">Надым</option>
                                                    <option value="413" data-select2-id="646">Назарово</option>
                                                    <option value="212" data-select2-id="647">Назрань</option>
                                                    <option value="642" data-select2-id="648">Называевск</option>
                                                    <option value="237" data-select2-id="649">Нальчик</option>
                                                    <option value="42" data-select2-id="650">Нариманов</option>
                                                    <option value="537" data-select2-id="651">Наро-Фоминск</option>
                                                    <option value="238" data-select2-id="652">Нарткала</option>
                                                    <option value="587" data-select2-id="653">Нарьян-Мар</option>
                                                    <option value="708" data-select2-id="654">Находка</option>
                                                    <option value="716" data-select2-id="655">Невель</option>
                                                    <option value="807" data-select2-id="656">Невельск</option>
                                                    <option value="896" data-select2-id="657">Невинномысск</option>
                                                    <option value="844" data-select2-id="658">Невьянск</option>
                                                    <option value="948" data-select2-id="659">Нелидово</option>
                                                    <option value="253" data-select2-id="660">Неман</option>
                                                    <option value="365" data-select2-id="661">Нерехта</option>
                                                    <option value="186" data-select2-id="662">Нерчинск</option>
                                                    <option value="1083" data-select2-id="663">Нерюнгри</option>
                                                    <option value="254" data-select2-id="664">Нестеров</option>
                                                    <option value="764" data-select2-id="665">Нефтегорск</option>
                                                    <option value="56" data-select2-id="666">Нефтекамск</option>
                                                    <option value="897" data-select2-id="667">Нефтекумск</option>
                                                    <option value="1020" data-select2-id="668">Нефтеюганск</option>
                                                    <option value="366" data-select2-id="669">Нея</option>
                                                    <option value="1021" data-select2-id="670">Нижневартовск</option>
                                                    <option value="929" data-select2-id="671">Нижнекамск</option>
                                                    <option value="224" data-select2-id="672">Нижнеудинск</option>
                                                    <option value="845" data-select2-id="673">Нижние Серги</option>
                                                    <option value="670" data-select2-id="674">Нижний Ломов</option>
                                                    <option value="846" data-select2-id="675">Нижний Тагил</option>
                                                    <option value="847" data-select2-id="676">Нижняя Салда</option>
                                                    <option value="848" data-select2-id="677">Нижняя Тура</option>
                                                    <option value="132" data-select2-id="678">Николаевск</option>
                                                    <option value="1007" data-select2-id="679">Николаевск-на-Амуре</option>
                                                    <option value="149" data-select2-id="680">Никольск (Вологодская область)</option>
                                                    <option value="671" data-select2-id="681">Никольск (Пензенская область)</option>
                                                    <option value="454" data-select2-id="682">Никольское</option>
                                                    <option value="455" data-select2-id="683">Новая Ладога</option>
                                                    <option value="849" data-select2-id="684">Новая Ляля</option>
                                                    <option value="898" data-select2-id="685">Новоалександровск</option>
                                                    <option value="12" data-select2-id="686">Новоалтайск</option>
                                                    <option value="133" data-select2-id="687">Новоаннинский</option>
                                                    <option value="162" data-select2-id="688">Нововоронеж</option>
                                                    <option value="32" data-select2-id="689">Новодвинск</option>
                                                    <option value="83" data-select2-id="690">Новозыбков</option>
                                                    <option value="385" data-select2-id="691">Новокубанск</option>
                                                    <option value="321" data-select2-id="692">Новокузнецк</option>
                                                    <option value="765" data-select2-id="693">Новокуйбышевск</option>
                                                    <option value="753" data-select2-id="694">Новомичуринск</option>
                                                    <option value="972" data-select2-id="695">Новомосковск</option>
                                                    <option value="899" data-select2-id="696">Новопавловск</option>
                                                    <option value="717" data-select2-id="697">Новоржев</option>
                                                    <option value="386" data-select2-id="698">Новороссийск</option>
                                                    <option value="663" data-select2-id="699">Новосиль</option>
                                                    <option value="718" data-select2-id="700">Новосокольники</option>
                                                    <option value="652" data-select2-id="701">Новотроицк</option>
                                                    <option value="793" data-select2-id="702">Новоузенск</option>
                                                    <option value="1000" data-select2-id="703">Новоульяновск</option>
                                                    <option value="850" data-select2-id="704">Новоуральск</option>
                                                    <option value="163" data-select2-id="705">Новохопёрск</option>
                                                    <option value="1070" data-select2-id="706">Новочебоксарск</option>
                                                    <option value="741" data-select2-id="707">Новочеркасск</option>
                                                    <option value="742" data-select2-id="708">Новошахтинск</option>
                                                    <option value="72" data-select2-id="709">Новый Оскол</option>
                                                    <option value="1095" data-select2-id="710">Новый Уренгой</option>
                                                    <option value="538" data-select2-id="711">Ногинск</option>
                                                    <option value="340" data-select2-id="712">Нолинск</option>
                                                    <option value="414" data-select2-id="713">Норильск</option>
                                                    <option value="1096" data-select2-id="714">Ноябрьск</option>
                                                    <option value="930" data-select2-id="715">Нурлат</option>
                                                    <option value="689" data-select2-id="716">Нытва</option>
                                                    <option value="1084" data-select2-id="717">Нюрба</option>
                                                    <option value="1022" data-select2-id="718">Нягань</option>
                                                    <option value="1048" data-select2-id="719">Нязепетровск</option>
                                                    <option value="33" data-select2-id="720">Няндома</option>
                                                    <option value="181" data-select2-id="721">Облучье</option>
                                                    <option value="284" data-select2-id="722">Обнинск</option>
                                                    <option value="433" data-select2-id="723">Обоянь</option>
                                                    <option value="635" data-select2-id="724">Обь</option>
                                                    <option value="539" data-select2-id="725">Одинцово</option>
                                                    <option value="540" data-select2-id="726">Ожерелье</option>
                                                    <option value="255" data-select2-id="727">Озёрск (Калининградская область)</option>
                                                    <option value="1049" data-select2-id="728">Озёрск (Челябинская область)</option>
                                                    <option value="541" data-select2-id="729">Озёры</option>
                                                    <option value="766" data-select2-id="730">Октябрьск</option>
                                                    <option value="57" data-select2-id="731">Октябрьский</option>
                                                    <option value="620" data-select2-id="732">Окуловка</option>
                                                    <option value="1085" data-select2-id="733">Олёкминск</option>
                                                    <option value="581" data-select2-id="734">Оленегорск</option>
                                                    <option value="303" data-select2-id="735">Олонец</option>
                                                    <option value="341" data-select2-id="736">Омутнинск</option>
                                                    <option value="34" data-select2-id="737">Онега</option>
                                                    <option value="719" data-select2-id="738">Опочка</option>
                                                    <option value="664" data-select2-id="739">Орёл</option>
                                                    <option value="653" data-select2-id="740">Оренбург</option>
                                                    <option value="542" data-select2-id="741">Орехово-Зуево</option>
                                                    <option value="342" data-select2-id="742">Орлов</option>
                                                    <option value="654" data-select2-id="743">Орск</option>
                                                    <option value="690" data-select2-id="744">Оса</option>
                                                    <option value="322" data-select2-id="745">Осинники</option>
                                                    <option value="949" data-select2-id="746">Осташков</option>
                                                    <option value="720" data-select2-id="747">Остров</option>
                                                    <option value="582" data-select2-id="748">Островной</option>
                                                    <option value="164" data-select2-id="749">Острогожск</option>
                                                    <option value="456" data-select2-id="750">Отрадное</option>
                                                    <option value="767" data-select2-id="751">Отрадный</option>
                                                    <option value="808" data-select2-id="752">Оха</option>
                                                    <option value="691" data-select2-id="753">Оханск</option>
                                                    <option value="692" data-select2-id="754">Очёр</option>
                                                    <option value="607" data-select2-id="755">Павлово</option>
                                                    <option value="165" data-select2-id="756">Павловск (Воронежская область)</option>
                                                    <option value="778" data-select2-id="757">Павловск (Санкт-Петербург)</option>
                                                    <option value="543" data-select2-id="758">Павловский Посад</option>
                                                    <option value="134" data-select2-id="759">Палласовка</option>
                                                    <option value="1127" data-select2-id="760">Партенит</option>
                                                    <option value="709" data-select2-id="761">Партизанск</option>
                                                    <option value="1077" data-select2-id="762">Певек</option>
                                                    <option value="672" data-select2-id="763">Пенза</option>
                                                    <option value="608" data-select2-id="764">Первомайск</option>
                                                    <option value="851" data-select2-id="765">Первоуральск</option>
                                                    <option value="609" data-select2-id="766">Перевоз</option>
                                                    <option value="544" data-select2-id="767">Пересвет</option>
                                                    <option value="1103" data-select2-id="768">Переславль-Залесский</option>
                                                    <option value="621" data-select2-id="769">Пестово</option>
                                                    <option value="779" data-select2-id="770">Петергоф</option>
                                                    <option value="135" data-select2-id="771">Петров Вал</option>
                                                    <option value="794" data-select2-id="772">Петровск</option>
                                                    <option value="187" data-select2-id="773">Петровск-Забайкальский</option>
                                                    <option value="304" data-select2-id="774">Петрозаводск</option>
                                                    <option value="292" data-select2-id="775">Петропавловск-Камчатский</option>
                                                    <option value="424" data-select2-id="776">Петухово</option>
                                                    <option value="113" data-select2-id="777">Петушки</option>
                                                    <option value="353" data-select2-id="778">Печора</option>
                                                    <option value="721" data-select2-id="779">Печоры</option>
                                                    <option value="457" data-select2-id="780">Пикалёво</option>
                                                    <option value="256" data-select2-id="781">Пионерский</option>
                                                    <option value="305" data-select2-id="782">Питкяранта</option>
                                                    <option value="973" data-select2-id="783">Плавск</option>
                                                    <option value="1050" data-select2-id="784">Пласт</option>
                                                    <option value="200" data-select2-id="785">Плёс</option>
                                                    <option value="166" data-select2-id="786">Поворино</option>
                                                    <option value="545" data-select2-id="787">Подольск</option>
                                                    <option value="458" data-select2-id="788">Подпорожье</option>
                                                    <option value="1023" data-select2-id="789">Покачи</option>
                                                    <option value="114" data-select2-id="790">Покров</option>
                                                    <option value="1086" data-select2-id="791">Покровск</option>
                                                    <option value="852" data-select2-id="792">Полевской</option>
                                                    <option value="257" data-select2-id="793">Полесск</option>
                                                    <option value="323" data-select2-id="794">Полысаево</option>
                                                    <option value="583" data-select2-id="795">Полярные Зори</option>
                                                    <option value="584" data-select2-id="796">Полярный</option>
                                                    <option value="809" data-select2-id="797">Поронайск</option>
                                                    <option value="722" data-select2-id="798">Порхов</option>
                                                    <option value="768" data-select2-id="799">Похвистнево</option>
                                                    <option value="84" data-select2-id="800">Почеп</option>
                                                    <option value="877" data-select2-id="801">Починок</option>
                                                    <option value="1104" data-select2-id="802">Пошехонье</option>
                                                    <option value="258" data-select2-id="803">Правдинск</option>
                                                    <option value="201" data-select2-id="804">Приволжск</option>
                                                    <option value="264" data-select2-id="805">Приморск (Калининградская область)</option>
                                                    <option value="459" data-select2-id="806">Приморск (Ленинградская область)</option>
                                                    <option value="1131" data-select2-id="807">Приморский</option>
                                                    <option value="387" data-select2-id="808">Приморско-Ахтарск</option>
                                                    <option value="460" data-select2-id="809">Приозерск</option>
                                                    <option value="324" data-select2-id="810">Прокопьевск</option>
                                                    <option value="743" data-select2-id="811">Пролетарск</option>
                                                    <option value="546" data-select2-id="812">Протвино</option>
                                                    <option value="239" data-select2-id="813">Прохладный</option>
                                                    <option value="723" data-select2-id="814">Псков</option>
                                                    <option value="795" data-select2-id="815">Пугачёв</option>
                                                    <option value="306" data-select2-id="816">Пудож</option>
                                                    <option value="724" data-select2-id="817">Пустошка</option>
                                                    <option value="202" data-select2-id="818">Пучеж</option>
                                                    <option value="780" data-select2-id="819">Пушкин</option>
                                                    <option value="547" data-select2-id="820">Пушкино</option>
                                                    <option value="548" data-select2-id="821">Пущино</option>
                                                    <option value="725" data-select2-id="822">Пыталово</option>
                                                    <option value="1024" data-select2-id="823">Пыть-Ях</option>
                                                    <option value="900" data-select2-id="824">Пятигорск</option>
                                                    <option value="115" data-select2-id="825">Радужный (Владимирская область)</option>
                                                    <option value="1025" data-select2-id="826">Радужный (Ханты-Мансийский авт. окр.)</option>
                                                    <option value="20" data-select2-id="827">Райчихинск</option>
                                                    <option value="549" data-select2-id="828">Раменское</option>
                                                    <option value="908" data-select2-id="829">Рассказово</option>
                                                    <option value="853" data-select2-id="830">Ревда</option>
                                                    <option value="854" data-select2-id="831">Реж</option>
                                                    <option value="550" data-select2-id="832">Реутов</option>
                                                    <option value="950" data-select2-id="833">Ржев</option>
                                                    <option value="203" data-select2-id="834">Родники</option>
                                                    <option value="878" data-select2-id="835">Рославль</option>
                                                    <option value="167" data-select2-id="836">Россошь</option>
                                                    <option value="1105" data-select2-id="837">Ростов</option>
                                                    <option value="551" data-select2-id="838">Рошаль</option>
                                                    <option value="796" data-select2-id="839">Ртищево</option>
                                                    <option value="13" data-select2-id="840">Рубцовск</option>
                                                    <option value="879" data-select2-id="841">Рудня</option>
                                                    <option value="552" data-select2-id="842">Руза</option>
                                                    <option value="487" data-select2-id="843">Рузаевка</option>
                                                    <option value="1106" data-select2-id="844">Рыбинск</option>
                                                    <option value="754" data-select2-id="845">Рыбное</option>
                                                    <option value="434" data-select2-id="846">Рыльск</option>
                                                    <option value="755" data-select2-id="847">Ряжск</option>
                                                    <option value="756" data-select2-id="848">Рязань</option>
                                                    <option value="1115" data-select2-id="849">Саки</option>
                                                    <option value="58" data-select2-id="850">Салават</option>
                                                    <option value="325" data-select2-id="851">Салаир</option>
                                                    <option value="1097" data-select2-id="852">Салехард</option>
                                                    <option value="745" data-select2-id="853">Сальск</option>
                                                    <option value="488" data-select2-id="854">Саранск</option>
                                                    <option value="996" data-select2-id="855">Сарапул</option>
                                                    <option value="797" data-select2-id="856">Саратов</option>
                                                    <option value="610" data-select2-id="857">Саров</option>
                                                    <option value="757" data-select2-id="858">Сасово</option>
                                                    <option value="1051" data-select2-id="859">Сатка</option>
                                                    <option value="880" data-select2-id="860">Сафоново</option>
                                                    <option value="1012" data-select2-id="861">Саяногорск</option>
                                                    <option value="225" data-select2-id="862">Саянск</option>
                                                    <option value="259" data-select2-id="863">Светлогорск</option>
                                                    <option value="901" data-select2-id="864">Светлоград</option>
                                                    <option value="260" data-select2-id="865">Светлый</option>
                                                    <option value="461" data-select2-id="866">Светогорск</option>
                                                    <option value="226" data-select2-id="867">Свирск</option>
                                                    <option value="21" data-select2-id="868">Свободный</option>
                                                    <option value="726" data-select2-id="869">Себеж</option>
                                                    <option value="1110" data-select2-id="870">Севастополь</option>
                                                    <option value="810" data-select2-id="871">Северо-Курильск</option>
                                                    <option value="96" data-select2-id="872">Северобайкальск</option>
                                                    <option value="35" data-select2-id="873">Северодвинск</option>
                                                    <option value="585" data-select2-id="874">Североморск</option>
                                                    <option value="855" data-select2-id="875">Североуральск</option>
                                                    <option value="959" data-select2-id="876">Северск</option>
                                                    <option value="85" data-select2-id="877">Севск</option>
                                                    <option value="307" data-select2-id="878">Сегежа</option>
                                                    <option value="86" data-select2-id="879">Сельцо</option>
                                                    <option value="611" data-select2-id="880">Семёнов</option>
                                                    <option value="746" data-select2-id="881">Семикаракорск</option>
                                                    <option value="168" data-select2-id="882">Семилуки</option>
                                                    <option value="1001" data-select2-id="883">Сенгилей</option>
                                                    <option value="136" data-select2-id="884">Серафимович</option>
                                                    <option value="612" data-select2-id="885">Сергач</option>
                                                    <option value="553" data-select2-id="886">Сергиев Посад</option>
                                                    <option value="673" data-select2-id="887">Сердобск</option>
                                                    <option value="856" data-select2-id="888">Серов</option>
                                                    <option value="554" data-select2-id="889">Серпухов</option>
                                                    <option value="462" data-select2-id="890">Сертолово</option>
                                                    <option value="782" data-select2-id="891">Сестрорецк</option>
                                                    <option value="59" data-select2-id="892">Сибай</option>
                                                    <option value="1052" data-select2-id="893">Сим</option>
                                                    <option value="1120" data-select2-id="894">Симферополь</option>
                                                    <option value="22" data-select2-id="895">Сковородино</option>
                                                    <option value="758" data-select2-id="896">Скопин</option>
                                                    <option value="14" data-select2-id="897">Славгород</option>
                                                    <option value="261" data-select2-id="898">Славск</option>
                                                    <option value="388" data-select2-id="899">Славянск-на-Кубани</option>
                                                    <option value="463" data-select2-id="900">Сланцы</option>
                                                    <option value="343" data-select2-id="901">Слободской</option>
                                                    <option value="227" data-select2-id="902">Слюдянка</option>
                                                    <option value="881" data-select2-id="903">Смоленск</option>
                                                    <option value="1053" data-select2-id="904">Снежинск</option>
                                                    <option value="586" data-select2-id="905">Снежногорск</option>
                                                    <option value="116" data-select2-id="906">Собинка</option>
                                                    <option value="262" data-select2-id="907">Советск (Калининградская область)</option>
                                                    <option value="344" data-select2-id="908">Советск (Кировская область)</option>
                                                    <option value="980" data-select2-id="909">Советск (Тульская область)</option>
                                                    <option value="1008" data-select2-id="910">Советская Гавань</option>
                                                    <option value="1026" data-select2-id="911">Советский</option>
                                                    <option value="150" data-select2-id="912">Сокол</option>
                                                    <option value="367" data-select2-id="913">Солигалич</option>
                                                    <option value="694" data-select2-id="914">Соликамск</option>
                                                    <option value="555" data-select2-id="915">Солнечногорск</option>
                                                    <option value="655" data-select2-id="916">Соль-Илецк</option>
                                                    <option value="36" data-select2-id="917">Сольвычегодск</option>
                                                    <option value="622" data-select2-id="918">Сольцы</option>
                                                    <option value="656" data-select2-id="919">Сорочинск</option>
                                                    <option value="1013" data-select2-id="920">Сорск</option>
                                                    <option value="308" data-select2-id="921">Сортавала</option>
                                                    <option value="285" data-select2-id="922">Сосенский</option>
                                                    <option value="345" data-select2-id="923">Сосновка</option>
                                                    <option value="415" data-select2-id="924">Сосновоборск</option>
                                                    <option value="464" data-select2-id="925">Сосновый Бор</option>
                                                    <option value="354" data-select2-id="926">Сосногорск</option>
                                                    <option value="389" data-select2-id="927">Сочи</option>
                                                    <option value="286" data-select2-id="928">Спас-Деменск</option>
                                                    <option value="759" data-select2-id="929">Спас-Клепики</option>
                                                    <option value="674" data-select2-id="930">Спасск</option>
                                                    <option value="710" data-select2-id="931">Спасск-Дальний</option>
                                                    <option value="760" data-select2-id="932">Спасск-Рязанский</option>
                                                    <option value="1087" data-select2-id="933">Среднеколымск</option>
                                                    <option value="857" data-select2-id="934">Среднеуральск</option>
                                                    <option value="188" data-select2-id="935">Сретенск</option>
                                                    <option value="902" data-select2-id="936">Ставрополь</option>
                                                    <option value="556" data-select2-id="937">Старая Купавна</option>
                                                    <option value="623" data-select2-id="938">Старая Русса</option>
                                                    <option value="951" data-select2-id="939">Старица</option>
                                                    <option value="87" data-select2-id="940">Стародуб</option>
                                                    <option value="1130" data-select2-id="941">Старый Крым</option>
                                                    <option value="73" data-select2-id="942">Старый Оскол</option>
                                                    <option value="60" data-select2-id="943">Стерлитамак</option>
                                                    <option value="960" data-select2-id="944">Стрежевой</option>
                                                    <option value="74" data-select2-id="945">Строитель</option>
                                                    <option value="117" data-select2-id="946">Струнино</option>
                                                    <option value="557" data-select2-id="947">Ступино</option>
                                                    <option value="974" data-select2-id="948">Суворов</option>
                                                    <option value="1129" data-select2-id="949">Судак</option>
                                                    <option value="435" data-select2-id="950">Суджа</option>
                                                    <option value="118" data-select2-id="951">Судогда</option>
                                                    <option value="119" data-select2-id="952">Суздаль</option>
                                                    <option value="309" data-select2-id="953">Суоярви</option>
                                                    <option value="88" data-select2-id="954">Сураж</option>
                                                    <option value="1027" data-select2-id="955">Сургут</option>
                                                    <option value="137" data-select2-id="956">Суровикино</option>
                                                    <option value="675" data-select2-id="957">Сурск</option>
                                                    <option value="478" data-select2-id="958">Сусуман</option>
                                                    <option value="287" data-select2-id="959">Сухиничи</option>
                                                    <option value="858" data-select2-id="960">Сухой Лог</option>
                                                    <option value="770" data-select2-id="961">Сызрань</option>
                                                    <option value="355" data-select2-id="962">Сыктывкар</option>
                                                    <option value="859" data-select2-id="963">Сысерть</option>
                                                    <option value="882" data-select2-id="964">Сычёвка</option>
                                                    <option value="465" data-select2-id="965">Сясьстрой</option>
                                                    <option value="860" data-select2-id="966">Тавда</option>
                                                    <option value="747" data-select2-id="967">Таганрог</option>
                                                    <option value="326" data-select2-id="968">Тайга</option>
                                                    <option value="228" data-select2-id="969">Тайшет</option>
                                                    <option value="558" data-select2-id="970">Талдом</option>
                                                    <option value="861" data-select2-id="971">Талица</option>
                                                    <option value="909" data-select2-id="972">Тамбов</option>
                                                    <option value="644" data-select2-id="973">Тара</option>
                                                    <option value="1098" data-select2-id="974">Тарко-Сале</option>
                                                    <option value="288" data-select2-id="975">Таруса</option>
                                                    <option value="636" data-select2-id="976">Татарск</option>
                                                    <option value="327" data-select2-id="977">Таштагол</option>
                                                    <option value="952" data-select2-id="978">Тверь</option>
                                                    <option value="294" data-select2-id="979">Теберда</option>
                                                    <option value="204" data-select2-id="980">Тейково</option>
                                                    <option value="489" data-select2-id="981">Темников</option>
                                                    <option value="390" data-select2-id="982">Темрюк</option>
                                                    <option value="240" data-select2-id="983">Терек</option>
                                                    <option value="931" data-select2-id="984">Тетюши</option>
                                                    <option value="391" data-select2-id="985">Тимашёвск</option>
                                                    <option value="466" data-select2-id="986">Тихвин</option>
                                                    <option value="392" data-select2-id="987">Тихорецк</option>
                                                    <option value="988" data-select2-id="988">Тобольск</option>
                                                    <option value="637" data-select2-id="989">Тогучин</option>
                                                    <option value="771" data-select2-id="990">Тольятти</option>
                                                    <option value="811" data-select2-id="991">Томари</option>
                                                    <option value="1088" data-select2-id="992">Томмот</option>
                                                    <option value="961" data-select2-id="993">Томск</option>
                                                    <option value="328" data-select2-id="994">Топки</option>
                                                    <option value="953" data-select2-id="995">Торжок</option>
                                                    <option value="954" data-select2-id="996">Торопец</option>
                                                    <option value="467" data-select2-id="997">Тосно</option>
                                                    <option value="151" data-select2-id="998">Тотьма</option>
                                                    <option value="1054" data-select2-id="999">Трёхгорный</option>
                                                    <option value="492" data-select2-id="1000">Троицк (Москва)</option>
                                                    <option value="1055" data-select2-id="1001">Троицк (Челябинская область)</option>
                                                    <option value="89" data-select2-id="1002">Трубчевск</option>
                                                    <option value="393" data-select2-id="1003">Туапсе</option>
                                                    <option value="61" data-select2-id="1004">Туймазы</option>
                                                    <option value="975" data-select2-id="1005">Тула</option>
                                                    <option value="229" data-select2-id="1006">Тулун</option>
                                                    <option value="983" data-select2-id="1007">Туран</option>
                                                    <option value="862" data-select2-id="1008">Туринск</option>
                                                    <option value="1107" data-select2-id="1009">Тутаев</option>
                                                    <option value="23" data-select2-id="1010">Тында</option>
                                                    <option value="241" data-select2-id="1011">Тырныауз</option>
                                                    <option value="645" data-select2-id="1012">Тюкалинск</option>
                                                    <option value="989" data-select2-id="1013">Тюмень</option>
                                                    <option value="910" data-select2-id="1014">Уварово</option>
                                                    <option value="812" data-select2-id="1015">Углегорск</option>
                                                    <option value="1108" data-select2-id="1016">Углич</option>
                                                    <option value="1089" data-select2-id="1017">Удачный</option>
                                                    <option value="955" data-select2-id="1018">Удомля</option>
                                                    <option value="416" data-select2-id="1019">Ужур</option>
                                                    <option value="976" data-select2-id="1020">Узловая</option>
                                                    <option value="97" data-select2-id="1021">Улан-Удэ</option>
                                                    <option value="1002" data-select2-id="1022">Ульяновск</option>
                                                    <option value="90" data-select2-id="1023">Унеча</option>
                                                    <option value="1028" data-select2-id="1024">Урай</option>
                                                    <option value="613" data-select2-id="1025">Урень</option>
                                                    <option value="346" data-select2-id="1026">Уржум</option>
                                                    <option value="1064" data-select2-id="1027">Урус-Мартан</option>
                                                    <option value="138" data-select2-id="1028">Урюпинск</option>
                                                    <option value="356" data-select2-id="1029">Усинск</option>
                                                    <option value="475" data-select2-id="1030">Усмань</option>
                                                    <option value="695" data-select2-id="1031">Усолье</option>
                                                    <option value="230" data-select2-id="1032">Усолье-Сибирское</option>
                                                    <option value="711" data-select2-id="1033">Уссурийск</option>
                                                    <option value="295" data-select2-id="1034">Усть-Джегута</option>
                                                    <option value="231" data-select2-id="1035">Усть-Илимск</option>
                                                    <option value="1056" data-select2-id="1036">Усть-Катав</option>
                                                    <option value="232" data-select2-id="1037">Усть-Кут</option>
                                                    <option value="394" data-select2-id="1038">Усть-Лабинск</option>
                                                    <option value="152" data-select2-id="1039">Устюжна</option>
                                                    <option value="357" data-select2-id="1040">Ухта</option>
                                                    <option value="63" data-select2-id="1041">Учалы</option>
                                                    <option value="417" data-select2-id="1042">Уяр</option>
                                                    <option value="436" data-select2-id="1043">Фатеж</option>
                                                    <option value="1128" data-select2-id="1044">Феодосия</option>
                                                    <option value="91" data-select2-id="1045">Фокино (Брянская область)</option>
                                                    <option value="712" data-select2-id="1046">Фокино (Приморский край)</option>
                                                    <option value="1125" data-select2-id="1047">Форос</option>
                                                    <option value="139" data-select2-id="1048">Фролово</option>
                                                    <option value="559" data-select2-id="1049">Фрязино</option>
                                                    <option value="205" data-select2-id="1050">Фурманов</option>
                                                    <option value="1009" data-select2-id="1051">Хабаровск</option>
                                                    <option value="395" data-select2-id="1052">Хадыженск</option>
                                                    <option value="1029" data-select2-id="1053">Ханты-Мансийск</option>
                                                    <option value="43" data-select2-id="1054">Харабали</option>
                                                    <option value="153" data-select2-id="1055">Харовск</option>
                                                    <option value="178" data-select2-id="1056">Хасавюрт</option>
                                                    <option value="798" data-select2-id="1057">Хвалынск</option>
                                                    <option value="189" data-select2-id="1058">Хилок</option>
                                                    <option value="560" data-select2-id="1059">Химки</option>
                                                    <option value="624" data-select2-id="1060">Холм</option>
                                                    <option value="813" data-select2-id="1061">Холмск</option>
                                                    <option value="561" data-select2-id="1062">Хотьково</option>
                                                    <option value="1071" data-select2-id="1063">Цивильск</option>
                                                    <option value="748" data-select2-id="1064">Цимлянск</option>
                                                    <option value="984" data-select2-id="1065">Чадан</option>
                                                    <option value="696" data-select2-id="1066">Чайковский</option>
                                                    <option value="772" data-select2-id="1067">Чапаевск</option>
                                                    <option value="476" data-select2-id="1068">Чаплыгин</option>
                                                    <option value="1057" data-select2-id="1069">Чебаркуль</option>
                                                    <option value="1072" data-select2-id="1070">Чебоксары</option>
                                                    <option value="242" data-select2-id="1071">Чегем</option>
                                                    <option value="977" data-select2-id="1072">Чекалин</option>
                                                    <option value="697" data-select2-id="1073">Чердынь</option>
                                                    <option value="233" data-select2-id="1074">Черемхово</option>
                                                    <option value="638" data-select2-id="1075">Черепаново</option>
                                                    <option value="154" data-select2-id="1076">Череповец</option>
                                                    <option value="296" data-select2-id="1077">Черкесск</option>
                                                    <option value="698" data-select2-id="1078">Чёрмоз</option>
                                                    <option value="562" data-select2-id="1079">Черноголовка</option>
                                                    <option value="1014" data-select2-id="1080">Черногорск</option>
                                                    <option value="1116" data-select2-id="1081">Черноморское</option>
                                                    <option value="699" data-select2-id="1082">Чернушка</option>
                                                    <option value="263" data-select2-id="1083">Черняховск</option>
                                                    <option value="563" data-select2-id="1084">Чехов</option>
                                                    <option value="932" data-select2-id="1085">Чистополь</option>
                                                    <option value="190" data-select2-id="1086">Чита</option>
                                                    <option value="614" data-select2-id="1087">Чкаловск</option>
                                                    <option value="625" data-select2-id="1088">Чудово</option>
                                                    <option value="639" data-select2-id="1089">Чулым</option>
                                                    <option value="700" data-select2-id="1090">Чусовой</option>
                                                    <option value="368" data-select2-id="1091">Чухлома</option>
                                                    <option value="985" data-select2-id="1092">Шагонар</option>
                                                    <option value="425" data-select2-id="1093">Шадринск</option>
                                                    <option value="1065" data-select2-id="1094">Шали</option>
                                                    <option value="418" data-select2-id="1095">Шарыпово</option>
                                                    <option value="369" data-select2-id="1096">Шарья</option>
                                                    <option value="564" data-select2-id="1097">Шатура</option>
                                                    <option value="814" data-select2-id="1098">Шахтёрск</option>
                                                    <option value="749" data-select2-id="1099">Шахты</option>
                                                    <option value="615" data-select2-id="1100">Шахунья</option>
                                                    <option value="761" data-select2-id="1101">Шацк</option>
                                                    <option value="75" data-select2-id="1102">Шебекино</option>
                                                    <option value="234" data-select2-id="1103">Шелехов</option>
                                                    <option value="37" data-select2-id="1104">Шенкурск</option>
                                                    <option value="191" data-select2-id="1105">Шилка</option>
                                                    <option value="24" data-select2-id="1106">Шимановск</option>
                                                    <option value="799" data-select2-id="1107">Шиханы</option>
                                                    <option value="468" data-select2-id="1108">Шлиссельбург</option>
                                                    <option value="1073" data-select2-id="1109">Шумерля</option>
                                                    <option value="426" data-select2-id="1110">Шумиха</option>
                                                    <option value="206" data-select2-id="1111">Шуя</option>
                                                    <option value="978" data-select2-id="1112">Щёкино</option>
                                                    <option value="1134" data-select2-id="1113">Щёлкино</option>
                                                    <option value="565" data-select2-id="1114">Щёлково</option>
                                                    <option value="493" data-select2-id="1115">Щербинка</option>
                                                    <option value="437" data-select2-id="1116">Щигры</option>
                                                    <option value="427" data-select2-id="1117">Щучье</option>
                                                    <option value="566" data-select2-id="1118">Электрогорск</option>
                                                    <option value="567" data-select2-id="1119">Электросталь</option>
                                                    <option value="568" data-select2-id="1120">Электроугли</option>
                                                    <option value="267" data-select2-id="1121">Элиста</option>
                                                    <option value="800" data-select2-id="1122">Энгельс</option>
                                                    <option value="169" data-select2-id="1123">Эртиль</option>
                                                    <option value="569" data-select2-id="1124">Юбилейный</option>
                                                    <option value="1030" data-select2-id="1125">Югорск</option>
                                                    <option value="207" data-select2-id="1126">Южа</option>
                                                    <option value="815" data-select2-id="1127">Южно-Сахалинск</option>
                                                    <option value="179" data-select2-id="1128">Южно-Сухокумск</option>
                                                    <option value="1059" data-select2-id="1129">Южноуральск</option>
                                                    <option value="329" data-select2-id="1130">Юрга</option>
                                                    <option value="120" data-select2-id="1131">Юрьев-Польский</option>
                                                    <option value="208" data-select2-id="1132">Юрьевец</option>
                                                    <option value="1060" data-select2-id="1133">Юрюзань</option>
                                                    <option value="289" data-select2-id="1134">Юхнов</option>
                                                    <option value="1074" data-select2-id="1135">Ядрин</option>
                                                    <option value="1090" data-select2-id="1136">Якутск</option>
                                                    <option value="1122" data-select2-id="1137">Ялта</option>
                                                    <option value="990" data-select2-id="1138">Ялуторовск</option>
                                                    <option value="64" data-select2-id="1139">Янаул</option>
                                                    <option value="347" data-select2-id="1140">Яранск</option>
                                                    <option value="15" data-select2-id="1141">Яровое</option>
                                                    <option value="1109" data-select2-id="1142">Ярославль</option>
                                                    <option value="883" data-select2-id="1143">Ярцево</option>
                                                    <option value="979" data-select2-id="1144">Ясногорск</option>
                                                    <option value="657" data-select2-id="1145">Ясный</option>
                                                    <option value="570" data-select2-id="1146">Яхрома</option>
                                                    </select>
                                                
                                            </label>
                                    </div>
                                    
                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Телефон<span class="field_required">*</span>
                                            </div>
                                            <input name="Phone" placeholder="Введите телефон" type="tel">
                                            <div class="field_item__message">
                                                <div class="field_item__message__title">На указанный номер мы отправим код для подтверждения регистрации</div>
                                            </div>
                                            <div class="field_item__message field_item__message--error">
                                                <div class="field_item__message__title">Данный номер телефона подтвержден другим участником программы.</div>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Email<span class="field_required">*</span>
                                            </div>
                                            <input name="Email" placeholder="Введите email" type="text">
                                        </label>
                                    </div>

                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Дата рождения<span class="field_required">*</span>
                                            </div>
                                            <input class="js_date_input date_input" name="dateBirthday" placeholder="Введите дату" type="text" autocomplete='off'>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="myaccount__edit_block">
                                <div class="myaccount__block__title myaccount__block__title__2">Данные детей</div>
                                    <div class="field_item field_item--child field_item--child--4">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Дата рождения ребёнка<span class="field_required">*</span>
                                            </div>
                                            <input class="js_date_child js_date_input date_input" name="ChildrenBirthdays[0]" placeholder="Введите дату" type="text" autocomplete='off'>
                                        </label>
                                        <button class="js_btn_child__field btn_default btn_gold btn_add_child">Добавить ребенка</button>
                                    </div>
                                    <div class="field_item field_item--submit">
                                        <button class="btn_default btn_red btn_edit_user_info">Сохранить</button>
                                    </div>

                                    
                            </div>
                        </form>

                        <div class="myaccount__edit_block">
                            <div class="myaccount__block__title myaccount__block__title__2">Социальные сети</div>
                            <div class="edit__social_links">
                                <a href="javascript:void(0)" class="edit__social_link">
                                    <img src="images/icons/social/default/vk.svg">
                                    <div class="edit__social_link__title">Добавить аккаунт</div>
                                    <div class="edit__social_link__btn">+</div>
                                </a>
                                <a href="javascript:void(0)" class="edit__social_link">
                                    <img src="images/icons/social/default/fb.svg">
                                    <div class="edit__social_link__title">Добавить аккаунт</div>
                                    <div class="edit__social_link__btn">+</div>
                                </a>
                                <a href="javascript:void(0)" class="edit__social_link">
                                    <img src="images/icons/social/default/ok.svg">
                                    <div class="edit__social_link__title">Добавить аккаунт</div>
                                    <div class="edit__social_link__btn">+</div>
                                </a>
                                <a href="javascript:void(0)" class="edit__social_link">
                                    <img src="images/icons/social/default/gg.svg">
                                    <div class="edit__social_link__title">Добавить аккаунт</div>
                                    <div class="edit__social_link__btn">+</div>
                                </a>
                            </div>
                        </div>
                        <div class="myaccount__edit_block">
                            <div class="myaccount__edit_block__line">
                                <div class="myaccount__block__title myaccount__block__title__2">Смена пароля</div>
                                <a href="javascript:void(0)" class="btn_def btn_gold btn_edit">запросить смену пароля</a>
                            </div>
                        </div>
                        <div class="myaccount__edit_block myaccount__edit_block--last">
                            <div class="myaccount__edit_block__line">
                                <div class="myaccount__block__title myaccount__block__title__2">Удаление аккаунта</div>
                                <a href="javascript:void(0)" class="btn_def btn_gold btn_edit">Удалить аккаунт</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            

            




            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
