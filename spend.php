<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>

            <?php include 'parts/myaccount/header.php'; ?>

            <?php include 'parts/myaccount/menu.php'; ?>

            <div class="myaccount__content">
                <div class="container">
                    <div class="myaccount__block myaccount__block__first myaccount__block_spend_block">
                            <div class="myaccount__block_spend_top">
                                <div class="myaccount__block_spend_top_title">Все Магазины</div>
                                <div class="myaccount__block_spend_top_list js_myaccount__block_spend_top_list"> 
                                    <a href="javascript:void(0)">
                                        <img src="images/logo/logo--huggies.png">
                                    </a>
                                    <a href="javascript:void(0)" class='active'>
                                        <img src="images/logo/partners/1.svg">
                                    </a>
                                    <a href="javascript:void(0)">
                                        <img src="images/logo/partners/2.svg">
                                    </a>
                                </div>
                            </div>
                            
                            <?php include 'parts/myaccount/spend_filter_mobile.php'; ?>

                            <div class="myaccount__block_spend_body">
                                <div class="myaccount__block_spend_inner js_myaccount__block_spend_1">
                                    <div class="myaccount__block_spend_body_top">Подарки от Детского Мира</div>
                                    <?php include 'parts/myaccount/spend_filter.php'; ?>
                                    <div class="myaccount__block_spend_column js_spend_content">

                                        <div class="spend_content spend_content--1">
                                            <div class="myaccount__block_spend_content">
                                                <?php for ($i=1; $i < 6; $i++) :?>
                                                    <div class="product_item_wrap ">
                                                        <a href="#" class='product_image_wrap_modal'>
                                                            <img src="images/prize/<?= $i;?>.jpg">
                                                        </a>
                                                        <div class="product_title_modal">Игрушка Baby Go Лев 2</div>
                                                        <div class="product_title_count">50 хагов</div>
                                                        <a href="#" class="btn_default btn_exchange">Обменять</a>
                                                    </div>
                                                <?php endfor;?>
                                            </div>
                                        </div>

                                            

                                        <div class="spend_content spend_content--2 active">
                                            <div class="myaccount__block_spend_content">
                                                <?php for ($i=1; $i < 9; $i++) :?>
                                                    <div class="product_item_wrap ">
                                                        <a href="#" class='product_image_wrap_modal'>
                                                            <img src="images/prize/<?= $i;?>.jpg">
                                                        </a>
                                                        <div class="product_title_modal">Игрушка Baby Go Лев 2</div>
                                                        <div class="product_title_count">50 хагов</div>
                                                        <a href="#" class="btn_default btn_exchange">Обменять</a>
                                                    </div>
                                                <?php endfor;?>
                                            </div>
                                        </div>

                                        <div class="spend_content spend_content--3">
                                            <div class="myaccount__block_spend_content myaccount__block_spend_content--3">
                                                <?php for ($i=1; $i < 3; $i++) :?>
                                                    <div class="product_item_wrap ">
                                                        <a href="#" class='product_image_wrap_modal'>
                                                            <img src="images/prize/<?= $i;?>.jpg">
                                                        </a>
                                                        <div class="product_title_modal">Игрушка Baby Go Лев 2</div>
                                                        <div class="product_title_count">50 хагов</div>
                                                        <a href="#" class="btn_default btn_exchange">Обменять</a>
                                                    </div>
                                                <?php endfor;?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="myaccount__block myaccount__block--decore">
                        <div class="myaccount__block_how_title">Не можете разобраться, как начисляются Хаги? </div>
                        <div class="myaccount__block_how_text">Здесь мы подробно отвечаем на наиболее частые вопросы</div>
                        <a href="" class="btn_default btn_gold">хочу разобраться</a>
                    </div>
                </div>
            </div>

            

            




            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
