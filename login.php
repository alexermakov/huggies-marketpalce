<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>
            <div class="page__form_block__wrap">
                <div class="page__form_block page__form_login">
                    <div class="callback_title">Войти</div>
                    <div class="page__form_block__row page__form_block__row--2">
                        <div class="login_page__form_wrap login_page__form_wrap--left login_page__form_wrap__divide">
                            <div class="login_page__form">
                                <form action="">
                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Email<span class="field_required">*</span>
                                            </div>
                                            <input name="Email" placeholder="Введите email" type="text">
                                            <div class="field_item__message field_item__message--error">
                                                <div class="field_item__message__title">Это поле обязательно.</div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="field_item">
                                        <label class="field_item_label">
                                            <div class="field_item__title">
                                                Пароль<span class="field_required">*</span>
                                            </div>
                                            <input name="Name" placeholder="*******" type="password">
                                            <div class="field_item__message field_item__message--error">
                                                <div class="field_item__message__title">Это поле обязательно.</div>
                                            </div>
                                        </label>
                                    </div>
                                    <div class="block__remember_me block__checkbox">
                                        <label>
                                            <input type="checkbox" name="remember">
                                            <span class="block__checkbox__view"></span>
                                            <span class="block__checkbox__text">Запомнить меня</span>
                                        </label>
                                    </div>
                                    <a href="#" class="link_lost_password">Восстановить пароль</a>
                                    <div class="field_item field_item--submit">
                                        <button class="btn_default btn_red">Войти</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="login_page__form_wrap login_page__form_wrap--right login_page__form_wrap--social">
                            <div class="login_page__form__social ">
                                <form action="/Account/ExternalLogin">                            
                                    <div class="social__login__title">Войдите через социальную сеть</div>
                                    <a class="social__login social__login--vk">
                                        <img src="images/icons/social/login/vk.svg">
                                        <span>Vkontakte</span>
                                    </a>
                                    <a class="social__login social__login--fb">
                                        <img src="images/icons/social/login/fb.svg">
                                        <span>Facebook</span>
                                    </a>
                                    <a class="social__login social__login--gg">
                                        <img src="images/icons/social/login/gg.svg">
                                        <span>Google</span>
                                    </a>
                                    <a class="social__login social__login--ok">
                                        <img src="images/icons/social/login/ok.svg">
                                        <span>Odnoklassniki</span>
                                    </a>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="page__form_login__bottom">
                        Нет аккаунта? <a href="">Пройдите регистрацию</a>
                    </div>
                </div>
            </div>
            

            


            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
