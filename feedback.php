<html lang="ru-RU">

    <head>
        <title>Index</title>
        <?php include 'parts/head.php'; ?>
    </head>

    <body>
        <div class="wrap__x">
            <?php include 'parts/header.php'; ?>

            <div class="callback_block">
                <div class="container">
                    <div class="callback_block_wrap">
                        <div class="callback_block_top">
                            <div class="callback_title">
                                Обратная связь
                            </div>
                            <div class="callback_subtitle">Есть вопросы? Напишите нам!</div>
                            <form action="/feedback" enctype="multipart/form-data" method="post">

                                <div class="field_item__row field_item__row--2">
                                    <div class="field_item">
                                        <label class='field_item_label'>
                                            <div class="field_item__title">
                                                Имя<span class="field_required">*</span>
                                            </div>
                                            <input name="Name" placeholder="Введите имя" type="text">
                                            <div class="field_item__message field_item__message--error">
                                                <div class="field_item__message__title">Это поле обязательно.</div>
                                            </div>
                                        </label>
                                    </div>

                                    <div class="field_item">
                                        <label class='field_item_label'>
                                            <div class="field_item__title">
                                                Email<span class="field_required">*</span>
                                            </div>
                                            <input name="Email" placeholder="Введите email" type="text">
                                            <div class="field_item__message field_item__message--error">
                                                <div class="field_item__message__title">Пожалуйста, введите корректный e-mail.</div>
                                            </div>
                                        </label>
                                    </div>
                                </div>

                                <div class="field_item__row field_item__row--2 ">
                                    <div class="field_item">
                                        <label class='field_item_label'>
                                            <div class="field_item__title">
                                                Телефон
                                            </div>
                                            <input name="Phone" placeholder="Введите телефон" type="text">
                                        </label>
                                    </div>

                                    <div class="field_item field_item--textarea">
                                        <label class='field_item_label'>
                                            <div class="field_item__title">
                                                Сообщение<span class="field_required">*</span>
                                            </div>
                                            <textarea cols="20" rows="2"></textarea>
                                        </label>
                                    </div>

                                    <div class="field_item field_item--file field_item--file__feedback">
                                        <label class='field_item_label'>
                                            <div class="field_item__title--file">
                                                Добавить файл <span>не более 10мб</span>
                                            </div>
                                            <input type="file" name="File" id="feedback-file">
                                        </label>
                                    </div>
                                </div>

                                <div class="field_item__row">
                                    <div class="agree_callback">
                                        <label> <input type="checkbox" name="AcceptRules" required checked>
                                            <span>Принимаю пользовательное <a href="javascript:void(0)">соглашение и даю согласие на обработку своих персональных данных*</a></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="field_item__row field_item__row--btn_callback">
                                    <button class="btn_default btn_red btn_callback_send">отправить</button>
                                </div>
                            </form>
                        </div>
                        <div class="callback_block_bottom">
                            <p>* Обязательные для заполнения поля</p>
                        </div>
                    </div>
                </div>
            </div>


            <?php include 'parts/footer.php'; ?>
        </div>
    </body>

</html>
