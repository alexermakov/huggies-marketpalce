<html lang="ru-RU">

	<head>
		<title>Index</title>
		<?php include 'parts/head.php'; ?>
	</head>

	<body>
		<div class="wrap__x">
			<?php include 'parts/header.php'; ?>


		

			<section class="home_banner">
				<div class="container">
					<div class="home_banner__info">
						<div class="home_banner__title">Добро пожаловать <br>в программу лояльности</div>
						<img class="home_banner__logo" src="images/logo/logo_big.png">
						<div class="home_banner__partners">
							<div class="home_banner__partners_title">Официальные партнеры</div>
							<div class="home_banner__partners_list">
								<img src="images/logo/partners/1.svg">
								<img src="images/logo/partners/2.svg">
							</div>
						</div>
					</div>

					<div class="home_triggers">
						<div class="home_trigger">
							<img src="images/index/triggers/1.svg" class='home_trigger__image'>
							<div class="home_trigger__info">Регистрируйтесь</div>
						</div>
						<div class="home_trigger">
							<img src="images/index/triggers/2.svg" class='home_trigger__image'>
							<div class="home_trigger__info">Покупайте<br> Huggies</div>
						</div>
						<div class="home_trigger">
							<img src="images/index/triggers/3.svg" class='home_trigger__image'>
							<div class="home_trigger__info">Загружайте чеки<br>и копите «хаги»</div>
						</div>
						<div class="home_trigger">
							<img src="images/index/triggers/4.svg" class='home_trigger__image'>
							<div class="home_trigger__info">Обменивайте хаги <br>на скидки и призы </div>
						</div>
					</div>
				</div>
			</section>



			<section class="home_page__prize">
				<div class="container">
					<div class="home_page__prize_wrap">
						<div class="home_page__prize_top">
							<div class="home_page__prize_title">Копи Хаги и меняй на скидки и классные призы!</div>
							<div class="home_page__prize_subtitle">Каждые потраченные 100 рублей на Huggies  &nbsp;= <span><strong>1 хаг</strong></span></div>
						</div>
						<a class="js_modal_catalog home_page__prize_link" data-options="{'touch' : false}" data-fancybox data-src="#modal_catalog_prize" href="#modal_catalog_prize">весь каталог призов</a>
						<div class="home_page__prize_items js_home_page__prize_items">
							<div class="home_page__prize_item">
								<img src="images/index/prize/1.svg" class="home_page__prize_item__bg">
							</div>
							<div class="home_page__prize_item">
								<img src="images/index/prize/2.svg" class="home_page__prize_item__bg">
								<img src="images/index/prize/image_2.png" class="home_page__prize_item__image">
							</div>
							<div class="home_page__prize_item">
								<img src="images/index/prize/3.svg" class="home_page__prize_item__bg">
								<img src="images/index/prize/image_3.png" class="home_page__prize_item__image">
							</div>
							<div class="home_page__prize_item">
								<img src="images/index/prize/4.svg" class="home_page__prize_item__bg">
								<img src="images/index/prize/image_4.png" class="home_page__prize_item__image">
							</div>
						</div>
						<p class="prize_block_text"><strong>*<span class="span_hug">Хаг</span></strong> — (от англ. «To hug» — обнимать) внутренняя валюта программы лояльности «Мой Huggies». Вы можете обменять накопленные хаги на подарки и скидки <a href="/user">в личном кабинете.</a></p>
					</div>
				</div>
			</section>




			<section class="home__calendar">
				<div class="container">
					<div class="home__calendar__info">
						<div class="home__calendar__title">Станьте Супер-мамой Huggies!</div>
						<div class="home__calendar__text">Загружайте чеки 6 месяцев, станьте <span>Супер-мамой</span> Huggies и получите специальный приз!</div>
					</div>
				</div>
			</section>



			<section class="home__earn_hug">
				<div class="container">
					<div class="home__earn_hug__wrap">
						<div class="home__earn_hug__title">Зарабатывайте дополнительные Хаги!</div>
						<div class="home__earn_hug__list js_home__earn_hug__list">
							<div class="home__earn_hug__item">
								<div class="home__earn_hug__item__wrap">
									<div class="home__earn_hug__item__icon">
										<img src="images/icons/earn_hug/1.svg">
									</div>
									<div class="home__earn_hug__item__image">
										<img src="images/index/earn_hug/1.jpg">
									</div>
									<div class="home__earn_hug__item__info">Вовлекайте <br>друзей<br> в программу</div>
									<div class="home__earn_hug__item__count">+10 хагов</div>
								</div>
							</div>
							<div class="home__earn_hug__item">
								<div class="home__earn_hug__item__wrap">
									<div class="home__earn_hug__item__icon">
										<img src="images/icons/earn_hug/2.svg">
									</div>
									<div class="home__earn_hug__item__image">
										<img src="images/index/earn_hug/2.jpg">
									</div>
									<div class="home__earn_hug__item__info">Делитесь <br>в социальных<br> сетях</div>
									<div class="home__earn_hug__item__count">+6 хагов</div>
								</div>
							</div>
							<div class="home__earn_hug__item">
								<div class="home__earn_hug__item__wrap">
									<div class="home__earn_hug__item__icon">
										<img src="images/icons/earn_hug/3.svg">
									</div>
									<div class="home__earn_hug__item__image">
										<img src="images/index/earn_hug/3.jpg">
									</div>
									<div class="home__earn_hug__item__info">Оставляйте<br> отзывы <br>о Huggies</div>
									<div class="home__earn_hug__item__count">+8 хагов</div>
								</div>
							</div>
							<div class="home__earn_hug__item">
								<div class="home__earn_hug__item__wrap">
									<div class="home__earn_hug__item__icon">
										<img src="images/icons/earn_hug/4.svg">
									</div>
									<div class="home__earn_hug__item__image">
										<img src="images/index/earn_hug/4.jpg">
									</div>
									<div class="home__earn_hug__item__info">Пройдите<br> опрос в личном<br> кабинете</div>
									<div class="home__earn_hug__item__count">+5 хагов</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>



			<section class="home_page__registration">
				<div class="container">
					<div class="home_page__registration__title">Зарегистрируйтесь и получите приветственную <span>скидку 5%*</span> на покупку Huggies в Детском мире</div>

					<div class="home_page__registration__wrap">
						<form action="/Account/ShowRegister" method="post" class="home_page__registration__form">
							<div class="home_page__registration__form__title">Регистрация:</div>
							<div class="home_page__registration__form_wrap">
								<input name="__RequestVerificationToken" type="hidden">
								<input type="text" name="FirstName" placeholder="Имя">
								<input type="text" name="LastName" placeholder="Фамилия">
								<input type="email" name="Email" placeholder="E-mail">
								<input type="submit" value="Зарегистрироваться" class='btn_def btn_red btn_home_registration'>
							</div>
						</form>

						<form action="/Account/ExternalLogin" method="post" class="home_page__registration__form_social">
							<input name="__RequestVerificationToken" type="hidden">
							<input class="js_provider" type="hidden" name="provider">
							<div class="home_page__registration__form__title">Войдите через:</div>
							<div class="home_page__registration__form_social__list">
								<div onclick="socialLogin('VKontakte')">
									<img src="images/icons/social/default/vk.svg">
								</div>
								<div onclick="socialLogin('Facebook')">
									<img src="images/icons/social/default/fb.svg">
								</div>
								<div onclick="socialLogin('Google')">
									<img src="images/icons/social/default/gg.svg">
								</div>
								<div onclick="socialLogin('Odnoklassniki')">
									<img src="images/icons/social/default/ok.svg">
								</div>
							</div>
						</form>
					</div>
					<div class="home_page__registration__add_text">* Скидка суммируется с другими регулярными акциями на Huggies в интернет-магазине detmir.ru. Возможны исключения для специальных механик.</div>
				</div>
			</section>




			<?php include 'parts/footer.php'; ?>
		</div>
	</body>

</html>
