$(function() {
    $('.js_btn_modal_close').click(function (event) {
        event.preventDefault();
        $.fancybox.close();
    });

    $('.js_select').selectric();
    $('.js_date_input').datepicker()

    $('.js_modal__catalog_categories a').click(function (e) { 
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_modal__catalog_categories a').removeClass('active');
            $(this).addClass('active');
            let index = $(this).index()+1;
            $('.modal__catalog_body .modal__catalog_item').slideUp(400);
            $('.modal__catalog_body .js_modal__catalog--'+index).slideDown(400,function(){
                $('.js_catalog').slick('refresh');
            })
        }
    });

    


    $('.js_choose_gender__item').click(function (e) { 

        $(this).parent().find('.js_choose_gender__item').removeClass('active');
        $(this).addClass('active');
        if ($(this).data('gender')=='man'){
            $('.js_choose_gender__item__view').addClass('man');
        }
        else{
            $('.js_choose_gender__item__view').removeClass('man');
        }
    });


    $('.js_btn_show_history_hugs').click(function (e) { 
        e.preventDefault();
        $('.js_hugs_history').slideToggle(400);                                  
    })



    $('.js_btn_child__field').click(function (e) { 
        let parent = $(this).parent();
        let count =  parent.find('.field_item_label').length;
        $(this).before(` 
        <label class="field_item_label">
            <div class="field_item__title">
                Дата рождения ребёнка<span class="field_required">*</span>
            </div>
            <input class="js_date_child js_date_input date_input" name="ChildrenBirthdays[${count}]" placeholder="Введите дату" type="text" autocomplete='off'>
        </label>
        `);
        $('.js_date_input').datepicker()

        e.preventDefault();
                                           

    })


    $('.js_choose_gender__item__view').click(function (e) { 
        e.preventDefault();
        
        $(this).toggleClass('man');
        let gender = $(this).hasClass('man') ? 'man' : 'wooman';
        let index = 0
        $(this).parents('.choose_gender_block').find('.js_choose_gender__item').removeClass('active');
        
        if (gender=='man'){ index = 1}

        let el = $($(this).parents('.choose_gender_block').find('.js_choose_gender__item')[index]);

        el.addClass('active').find('input').prop('checked', true);

        console.log(el)
        console.log(gender)
        console.log(index)



    //     $(this).parents('.login_gender_block').find('.login_gender_block_item')
    //     .removeClass('login_gender_block_item_active');
    // if ($(this).hasClass('chooseMan')) {
    //     $(this).removeClass('chooseMan').addClass('chooseWoman');
    //     $('.login_gender_block_woman').addClass('login_gender_block_item_active');
    //     $('.login_gender_block_woman input').prop('checked', true);
    // } else {
    //     $(this).removeClass('chooseWoman').addClass('chooseMan');
    //     $('.login_gender_block_man').addClass('login_gender_block_item_active');
    //     $('.login_gender_block_man input').prop('checked', true);
    // }
// });
       
    });


    $('.js_myaccount__block_spend_top_list a').click(function (e) { 
        e.preventDefault();
        if (!$(this).hasClass('active')){
            $('.js_myaccount__block_spend_top_list a').removeClass('active');
            $(this).addClass('active');
            let index = $(this).index()+1;
            $('.myaccount__block_spend_filter__shop img').attr('src',$(this).find('img').attr('src'))

            $('.js_spend_content .spend_content').slideUp(400);
            $('.js_spend_content .spend_content--'+index).slideDown(400);
        }
    });

    $('.js_btn_filter_catalog').click(function (e) { 
        e.preventDefault();
        $('.js_filter__spend_mobile__body').slideDown(400)
    });

    $('.js_btn_close_mobile_filter').click(function (e) { 
        e.preventDefault();
        $('.js_filter__spend_mobile__body').slideUp(400)
    });

    $('.js_btn_spend_mobile__body_shop_list_drop').click(function (e) { 
        e.preventDefault();
        $(this).parents('.spend_mobile__body_shop_list_view').toggleClass('active');
        $('.js_spend_mobile__body_shop_list').slideToggle(400)
    });

    $(document).on('click','.js_spend_mobile__body_shop_list a',function (e) { 
        e.preventDefault();
        let index = $(this).data('index');
        $('.js_spend_content .spend_content').slideUp(400);
        $('.js_spend_content .spend_content--'+index).slideDown(400);
        $('.js_btn_spend_mobile__body_shop_list_drop').trigger('click');
        $('.js_spend_mobile__body_shop_list').prepend($('.spend_mobile__body_shop_list_view a:first-child'));
        $('.spend_mobile__body_shop_list_view').prepend($(this));
       
    });

    

    


var paramSlick2 = {
    infinite: false,
    slidesToShow: 4,
    swipeToSlide: true,
    dots: true,
    responsive: [
        {
            breakpoint: 1000,
            settings: {
                slidesToShow: 3,
            }
        },
        {
            breakpoint: 760,
            settings: {
                slidesToShow: 2,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                arrows:false
            }
        }
    ]
};


    $(".js_modal_catalog").fancybox({
        touch:false,
        beforeShow: function () {
            $('.js_catalog').slick(paramSlick2);
        },
        afterClose: function () {
            $('.js_catalog').slick('unslick');
        }
    });

    function socialLogin(type) {
        $('.js_provider').val(type);
        $(this).parents('form').submit();
    }
    $('.js_btn_mobile_menu').click(function (event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $('.js_mobile_menu').fadeToggle(400);
    });
    $('.js_faq_btn').click(function (event) {
        $(this).parents('.faq_item').toggleClass('active');
        $(this).parents('.faq_item').find('.faq_item_info').slideToggle(400);
    });

    $('.js_myaccount__menu_mobile__select').click(function (event) {
        $(this).next().stop().slideToggle(400).toggleClass('active');
        $(this).toggleClass('active');
     
    });

    $('.js_btn_show_add_hugs_block').click(function (event) {
        event.preventDefault();
        var el = $('.myaccount__block__additional_hugs');
        $('.myaccount__load_check').slideUp();
        el.slideDown();
        $("html, body").animate({ scrollTop: $('.my_account__exchange').offset().top + $('.my_account__exchange').outerHeight() - $('.js_header').outerHeight()}, 1000);
    })

    $('.js_btn_load_check_my_account').click(function (event) {
        event.preventDefault();
        var el = $('.myaccount__load_check');
        $('.myaccount__block__additional_hugs').slideUp();
        el.slideDown();
        $("html, body").animate({ scrollTop: $('.my_account__exchange').offset().top + $('.my_account__exchange').outerHeight() - $('.js_header').outerHeight()}, 1000);
    })

    let notification_block = {
        el:$('.notifications_block'),

        open(){
            this.el.removeClass('hide');
            if ($(window).width()<420)
                $('.notification_bg').fadeIn(500)

        },

        close(){
            this.el.addClass('hide');
            $('.notification_bg').fadeOut(500)

        }
    };
    
    $(document).on('click','.js_open_notification',function(event){
        event.preventDefault();
        notification_block.open();
    })


    $(document).on('click','.js_notifications_block_close,.js_notification_bg',function(event){
        event.preventDefault();
        notification_block.close();
    })

    



var paramSlick = {
    dots: true,
    arrows: false,
    infinite: false,
    outerEdgeLimit: true,
    speed: 300,
    variableWidth: true,
    swipeToSlide: true,
}


     function doSliderIndex() {
        if ($(window).width() <= 1250) {
            if ($('.js_home_page__prize_items.slick-initialized').length == 0)
                $('.js_home_page__prize_items,.js_home__earn_hug__list').slick(paramSlick);
        }
        else {
            if ($('.js_home_page__prize_items.slick-initialized').length > 0)
                $('.js_home_page__prize_items,.js_home__earn_hug__list').slick('unslick');
        }
    }

    $(window).resize(function (event) {
        doSliderIndex();
    });


    doSliderIndex();
});

